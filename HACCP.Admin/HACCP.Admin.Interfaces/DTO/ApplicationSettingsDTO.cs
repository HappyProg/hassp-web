﻿using System;

namespace HACCP.Admin.Interfaces
{
    public class ApplicationSettingsDTO : IApplicationSettings
    {
        public string IikoURL { get; set; }
        public string IikoUsername { get; set; }
        public string IikoPassword { get; set; }
        public Guid? Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsDeleted { get; set; }
        public string Comment { get; set; }
        public long? IikoRecipeCardRevision { get; set; }
    }
}
