﻿using System;

namespace HACCP.Admin.Interfaces
{
    public class ApplicationUserDTO : IApplicationUser
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patron { get; set; }
        public string PasswordSalt { get; set; }
        public IApplicationRole Role { get; set; }
        public string Password { get; set; }
        public DateTime TokenExpired { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
