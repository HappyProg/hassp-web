﻿using System;

namespace HACCP.Admin.Interfaces
{
    public class ApplicationRoleDTO : IApplicationRole
    {
        public Guid Id { get; set; }
        public RoleType RoleType { get; set; }
    }
}
