﻿using System;

namespace HACCP.Admin.Interfaces
{
    public class RecipeItemDTO : IRecipeItem
    {
        public decimal AmountIn { get; set; }
        public decimal AmountMiddle { get; set; }
        public decimal AmountOut { get; set; }
        public IProduct Product { get; set; }
        public Guid ProductId { get; set; }
        public Guid? Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsDeleted { get; set; }
        public string Comment { get; set; }
    }
}
