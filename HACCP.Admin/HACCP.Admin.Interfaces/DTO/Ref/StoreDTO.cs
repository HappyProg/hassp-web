﻿using System;

namespace HACCP.Admin.Interfaces
{
    public class StoreDTO : IStore
    {        
        public string Name { get; set; }
        public int Index { get; }
        public Guid? Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsDeleted { get; set; }
        public string Comment { get; set; }
    }
}
