﻿using System;

namespace HACCP.Admin.Interfaces
{
    public class UnitMeasurementDTO : IUnitMeasurement
    {
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsDeleted { get; set; }
        public string Comment { get; set; }
        public Guid? Id { get; set; }
    }
}
