﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace HACCP.Admin.Interfaces
{
    public class ProductDTO : IProduct
    {
        public string Prefix { get; set; }
        public string Article { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal MinimumBalanceToClear { get; set; }
        public ProductType ProductType { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsDeleted { get; set; }
        public string Comment { get; set; }
        public Guid? Id { get; set; }
        public IProduct Parent { get; set; }
        public Guid? ParentId { get; set; }
        public ICollection<IProduct> Children { get; set; }
    }
}
