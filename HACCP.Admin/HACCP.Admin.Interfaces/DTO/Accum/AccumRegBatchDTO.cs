﻿using System;
using System.Collections.Generic;

namespace HACCP.Admin.Interfaces
{
    public class AccumRegBatchDTO : IAccumRegBatch
    {
        public IDocument ParentDoc { get; set; }
        public Guid ConsignmentId { get; set; }
        public IBatch Consignment { get; set; }
        public decimal Amount { get; set; }
        public ICollection<IAccumRegBatch> Parent { get; set; }
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public OperationType OperationType { get; set; }
    }
}
