﻿using System;

namespace HACCP.Admin.Interfaces
{
    public class DocumentProductItemDTO : IDocumentProductItem
    {
        public Guid? ProductId { get; set; }
        public IProduct Product { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public decimal VatPercent { get; set; }
        public decimal VatAmount { get; set; }
        public Guid? UnitMeasurementId { get; set; }
        public IUnitMeasurement UnitMeasurement { get; set; }
        public Guid? StoreId { get; set; }
        public IStore Store { get; set; }
        public Guid? Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsDeleted { get; set; }
        public string Comment { get; set; }
    }
}
