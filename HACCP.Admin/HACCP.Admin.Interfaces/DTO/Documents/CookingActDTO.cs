﻿using System;
using System.Collections.Generic;

namespace HACCP.Admin.Interfaces
{
    public class CookingActDTO : ICookingAct
    {
        public ICollection<IBatch> Batches { get; set; } = new List<IBatch>();
        public IRecipeCard RecipeCard { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public IApplicationUser Owner { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsDeleted { get; set; }
        public string Comment { get; set; }
        public Guid? Id { get; set; }
        public IBatch ResultBatch { get; set; }
        public decimal ResultAmount { get; set; }
        public IStore Store { get; set; }
        public Guid StoreId { get; set; }
    }
}
