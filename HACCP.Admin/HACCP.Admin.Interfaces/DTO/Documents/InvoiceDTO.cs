﻿using System;
using System.Collections.Generic;

namespace HACCP.Admin.Interfaces
{
    public class InvoiceDTO : IInvoice
    {
        public ICollection<IBatch> Batches { get; set; }
        public ICollection<IDocumentProductItem> Items { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public IApplicationUser Owner { get; set; }
        public Guid? Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsDeleted { get; set; }
        public string Comment { get; set; }
        public IStore Store { get; set; }
        public Guid StoreId { get; set; }
    }
}
