﻿using System;

namespace HACCP.Admin.Interfaces
{
    public class BatchDTO : IBatch
    {
        public DateTime Date { get; set; }
        public string Number { get; set; }
        public IProduct Product { get; set; }
        public string Prefix { get; set; }
        public int BatchIndex { get; set; }
        public decimal Amount { get; set; }
        public decimal CurrentAmount { get; set; }
        public IUnitMeasurement UnitMeasurement { get; set; }
        public IStore Store { get; set; }
        public bool IsSpent => CurrentAmount <= 0;
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsDeleted { get; set; }
        public string Comment { get; set; }
        public Guid? Id { get; set; }
    }
}
