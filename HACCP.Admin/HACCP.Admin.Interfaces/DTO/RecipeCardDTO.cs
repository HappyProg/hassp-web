﻿using System;
using System.Collections.Generic;

namespace HACCP.Admin.Interfaces
{
    public class RecipeCardDTO : IRecipeCard
    {
        public decimal Coefficient { get; set; }
        public ICollection<IRecipeItem> Items { get; set; }
        public IProduct ResultProduct { get; set; }
        public Guid? Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsDeleted { get; set; }
        public string Comment { get; set; }
        public Guid ResultProductId { get; set; }
        public int Revision { get; set; }
    }
}
