﻿using System;

namespace HACCP.Admin.Interfaces
{
    public interface IEqutableDTO<T> : IEquatable<T>
    {
    }
}
