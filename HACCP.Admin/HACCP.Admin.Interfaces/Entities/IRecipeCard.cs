﻿using System;
using System.Collections.Generic;

namespace HACCP.Admin.Interfaces
{
    public interface IRecipeCard : IBaseEntity
    {
        decimal Coefficient { get; set; }
        ICollection<IRecipeItem> Items { get; set; }
        IProduct ResultProduct { get; set; }
        Guid ResultProductId { get; set; }
        int Revision { get; set; }
    }
}