﻿namespace HACCP.Admin.Interfaces
{
    public interface IApplicationSettings : IBaseEntity
    {
        string IikoURL { get; set; }
        string IikoUsername { get; set; }
        string IikoPassword { get; set; }
        long? IikoRecipeCardRevision { get; set; }
    }
}
