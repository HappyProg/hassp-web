﻿using System;

namespace HACCP.Admin.Interfaces
{
    public interface IApplicationRole
    {
        Guid Id { get; set; }
        RoleType RoleType { get; set; }
    }
}
