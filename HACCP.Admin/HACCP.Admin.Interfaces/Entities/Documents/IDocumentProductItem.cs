﻿using System;

namespace HACCP.Admin.Interfaces
{
    public interface IDocumentProductItem : IBaseEntity
    {
        IProduct Product { get; set; }
        decimal Price { get; set; }
        decimal Quantity { get; set; }
        decimal VatPercent { get; set; }
        decimal VatAmount { get; set; }
        IUnitMeasurement UnitMeasurement { get; set; }
        /// <summary>
        /// Склад хранения
        /// </summary>
        IStore Store { get; set; }
        Guid? StoreId { get; set; }
        Guid? ProductId { get; set; }
        Guid? UnitMeasurementId { get; set; }
    }
}
