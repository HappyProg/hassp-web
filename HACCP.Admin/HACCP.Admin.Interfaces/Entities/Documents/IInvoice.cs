﻿using System.Collections.Generic;

namespace HACCP.Admin.Interfaces
{
    public interface IInvoice : IDocument
    {
        /// <summary>
        /// Партии
        /// </summary>
        ICollection<IBatch> Batches { get; set; }
        /// <summary>
        /// Товары
        /// </summary>
        ICollection<IDocumentProductItem> Items { get; set; }
    }
}
