﻿using System;

namespace HACCP.Admin.Interfaces
{
    public interface IDocument : IBaseEntity
    {
        string Number { get; set; }
        DateTime Date { get; set; }
        IApplicationUser Owner { get; set; }
        IStore Store { get; set; }
        Guid StoreId { get; set; }
    }
}
