﻿using System.Collections.Generic;

namespace HACCP.Admin.Interfaces
{
    public interface ICookingAct : IDocument
    {
        IBatch ResultBatch { get; set; }
        /// <summary>
        /// Партии
        /// </summary>
        ICollection<IBatch> Batches { get; set; }
        /// <summary>
        /// Тех. карта (рецепт)
        /// </summary>
        IRecipeCard RecipeCard { get; set; }
        decimal ResultAmount { get; set; }
    }
}
