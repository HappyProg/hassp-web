﻿using System;

namespace HACCP.Admin.Interfaces
{
    public interface IBaseAccumReg
    {
        Guid Id { get; set; }
        DateTime Date { get; set; }
        OperationType OperationType { get; set; }
    }
}
