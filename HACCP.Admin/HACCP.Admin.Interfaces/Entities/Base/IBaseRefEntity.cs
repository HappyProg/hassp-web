﻿namespace HACCP.Admin.Interfaces
{
    public interface IBaseRefEntity : IBaseEntity
    {
        string Name { get; set; }
    }
}
