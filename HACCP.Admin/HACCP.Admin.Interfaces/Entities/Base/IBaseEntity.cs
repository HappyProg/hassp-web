﻿using System;

namespace HACCP.Admin.Interfaces
{
    public interface IBaseEntity
    {
        Guid? Id { get; set; }
        DateTime DateCreated { get; set; }
        DateTime DateModified { get; set; }
        bool IsDeleted { get; set; }
        string Comment { get; set; }
    }
}
