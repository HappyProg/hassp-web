﻿using System.ComponentModel;

namespace HACCP.Admin.Interfaces
{
    public enum RoleType
    {
        [Description("Администратор")]
        Admin = 1,
        [Description("Пользователь")]
        User = 2
    }
}
