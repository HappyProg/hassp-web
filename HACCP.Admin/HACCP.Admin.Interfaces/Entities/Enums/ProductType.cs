﻿namespace HACCP.Admin.Interfaces
{
    public enum ProductType
    {
        NoSet = 0,
        /// <summary>
        /// Товар
        /// </summary>
        Goods = 1,
        /// <summary>
        /// Блюдо
        /// </summary>
        Dish = 2,
        /// <summary>
        /// Заготовка (полуфабрикат)
        /// </summary>
        Prepared = 3,
        /// <summary>
        /// Услуга
        /// </summary>
        Service = 4,
        /// <summary>
        /// Модификатор
        /// </summary>
        Modifier = 5
    }
}
