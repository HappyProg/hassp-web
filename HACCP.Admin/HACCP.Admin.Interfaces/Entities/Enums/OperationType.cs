﻿namespace HACCP.Admin.Interfaces
{
    public enum OperationType
    {
        /// <summary>
        /// Приход
        /// </summary>
        Inc = 1,
        /// <summary>
        /// Расход
        /// </summary>
        Out = 2,
        /// <summary>
        /// Готовка
        /// </summary>
        Cooking = 3,
        /// <summary>
        /// Продажа
        /// </summary>
        Sale = 4,
        /// <summary>
        /// Списание
        /// </summary>
        Substract = 5
    }
}
