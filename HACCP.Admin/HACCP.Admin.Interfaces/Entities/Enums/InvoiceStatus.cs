﻿namespace HACCP.Admin.Interfaces
{
    public enum InvoiceStatus
    {
        NoSet = 0,
        New = 1,
        Processed = 2,
        Deleted = 3
    }
}
