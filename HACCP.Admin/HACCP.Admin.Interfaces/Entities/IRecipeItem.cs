﻿using System;

namespace HACCP.Admin.Interfaces
{
    public interface IRecipeItem : IBaseEntity
    {
        decimal AmountIn { get; set; }
        decimal AmountMiddle { get; set; }
        decimal AmountOut { get; set; }
        IProduct Product { get; set; }
        Guid ProductId { get; set; }
    }
}