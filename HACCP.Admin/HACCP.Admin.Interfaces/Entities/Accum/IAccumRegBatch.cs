﻿using System;
using System.Collections.Generic;

namespace HACCP.Admin.Interfaces
{
    public interface IAccumRegBatch : IBaseAccumReg
    {
        IDocument ParentDoc { get; set; }
        /// <summary>
        /// Внешний ключ
        /// </summary>
        Guid ConsignmentId { get; set; }
        /// <summary>
        /// Партия
        /// </summary>
        IBatch Consignment { get; set; }
        decimal Amount { get; set; }
        ICollection<IAccumRegBatch> Parent { get; set; }
    }
}
