﻿using System;

namespace HACCP.Admin.Interfaces
{
    public interface IBatch : IBaseEntity
    {
        /// <summary>
        /// Дата партии (может отличаться от даты создания)
        /// </summary>
        DateTime Date { get; set; }
        string Number { get; set; }
        IProduct Product { get; set; }
        string Prefix { get; set; }
        /// <summary>
        /// Номер партии - сквозной, автоинкремент
        /// </summary>
        int BatchIndex { get; set; }
        ///// <summary>
        ///// Накладная
        ///// </summary>
        //public Invoice Invoice { get; set; }
        /// <summary>
        /// Количество
        /// </summary>
        decimal Amount { get; set; }
        /// <summary>
        /// Текущее кол-во, отражает кол-во партии с учетом всех списаний, готовок, корректировок и т.д.
        /// </summary>
        decimal CurrentAmount { get; set; }
        /// <summary>
        /// Единица измерения
        /// </summary>
        IUnitMeasurement UnitMeasurement { get; set; }
        /// <summary>
        /// Склад хранения
        /// </summary>
        IStore Store { get; set; }
        /// <summary>
        /// Признак списана ли партия
        /// </summary>
        bool IsSpent { get; }
    }
}
