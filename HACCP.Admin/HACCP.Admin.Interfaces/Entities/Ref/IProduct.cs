﻿using System;
using System.Collections.Generic;

namespace HACCP.Admin.Interfaces
{
    public interface IProduct : IBaseRefEntity
    {
        string Prefix { get; set; }
        string Article { get; set; }
        string Code { get; set; }
        /// <summary>
        /// Кол-во остатка для незамедлительного списания
        /// </summary>
        decimal MinimumBalanceToClear { get; set; }
        ProductType ProductType { get; set; }
        Guid? ParentId { get; set; }
        IProduct Parent { get; set; }
        ICollection<IProduct> Children { get; set; }        
    }
}
