﻿using System;

namespace HACCP.Admin.Interfaces
{
    public interface IApplicationUser
    {
        Guid Id { get; set; }
        string Username { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Patron { get; set; }
        string Password { get; set; }
        DateTime TokenExpired { get; set; }
        string PasswordSalt { get; set; }
        IApplicationRole Role { get; set; }
        string Email { get; set; }
    }
}
