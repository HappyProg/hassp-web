﻿using System.Threading.Tasks;

namespace HACCP.Admin.Interfaces
{
    public interface ICookingService
    {
        Task<ICookingAct> Create(IRecipeCard recipeCard, decimal amount, IStore store);
    }
}