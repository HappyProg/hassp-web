﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace HACCP.Admin.Interfaces
{
    public interface IRecipeCardService
    {
        Task<IEnumerable<IRecipeCard>> CreateAsync(IEnumerable<IRecipeCard> cards);
        Task<IRecipeCard> CreateAsync(IRecipeCard card);
    }
}