﻿using System.Threading.Tasks;

namespace HACCP.Admin.Interfaces
{
    public interface ISettingsService
    {
        Task<IApplicationSettings> GetAsync();
        Task SaveAsync(IApplicationSettings settings);
    }
}