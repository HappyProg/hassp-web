﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace HACCP.Admin.Interfaces
{
    public interface IProductService
    {
        Task<IEnumerable<IProduct>> GetAsync();

        /// <summary>
        /// Обновляет записи, если необходимо и возввращает только обновленные записи, если такие имеются
        /// </summary>
        /// <param name="products"></param>
        /// <returns>Обновленные / добавленные записи</returns>
        Task<IEnumerable<IProduct>> SaveAsync(IEnumerable<IProduct> products);
    }
}