﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HACCP.Admin.Interfaces
{
    public interface IExternalImportService
    {
        Task<IEnumerable<IRecipeCard>> GetRecipeCardsAsync(DateTime from, DateTime to, int revision = 0);
        Task<IEnumerable<IProduct>> GetProductCategoriesAsync();
        Task<IEnumerable<ICookingAct>> GetCookingActs(DateTime dateFrom, DateTime dateTo);
        Task<IEnumerable<IInvoice>> GetInvoicesAsync(DateTime dateFrom, DateTime dateTo);
        Task<IEnumerable<IProduct>> GetProductsAsync();
    }
}