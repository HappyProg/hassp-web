﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HACCP.Admin.Interfaces
{
    public interface IInvoiceService
    {
        Task<IInvoice> GetAsync(Guid id);
        Task<IEnumerable<IInvoice>> GetAsync();
        Task<IEnumerable<IInvoice>> LoadFromExternalServiceAsync(DateTime from, DateTime to);

        /// <summary>
        /// Обновляет записи, если необходимо и возввращает только обновленные записи, если такие имеются
        /// </summary>
        /// <param name="products"></param>
        /// <returns>Обновленные / добавленные записи</returns>
        Task<IEnumerable<IInvoice>> SaveAsync(IEnumerable<IInvoice> inv);
    }
}