﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HACCP.Admin.Interfaces
{
    public interface IUserService
    {
        Task<IApplicationUser> AuthenticateAsync(string username, string password);
        Task<IApplicationUser> CreateAsync(IApplicationUser user);
        Task<IEnumerable<IApplicationUser>> FindAsync(string searchStr = null);
        Task<IEnumerable<IApplicationUser>> GetAsync();
        Task<IApplicationUser> GetAsync(Guid id);
        Task<IApplicationUser> GetByNameAsync(string username);
    }
}