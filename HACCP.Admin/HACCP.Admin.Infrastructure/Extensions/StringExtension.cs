﻿using System.Text;

namespace HACCP.Admin.Infrastructure
{
    public static class StringExtension
    {
        static string[] rus = { "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й",
                "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц",
                "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я" };

        static string[] eng = { "A", "B", "V", "G", "D", "E", "E", "ZH", "Z", "I", "Y",
                "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "KH", "TS",
                "CH", "SH", "SHCH", "\"", "Y", "'", "E", "YU", "YA" };

        public static string Transliterate(this string str, int length = 0)
        {
            StringBuilder ret = new StringBuilder();

            for (int j = 0; j < str.Length; j++)
                for (int i = 0; i < rus.Length; i++)
                    if (str.Substring(j, 1).ToUpper() == rus[i])
                    {
                        ret.Append(eng[i]);
                        if (length > 0 && i == length - 1)
                        {
                            return ret.ToString();
                        }
                    }

            return ret.ToString();
        }
    }
}
