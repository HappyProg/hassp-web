﻿using System.Security.Cryptography;
using System.Text;

namespace HACCP.Admin.Infrastructure
{
    public enum HashType
    {
        MD5,
        SHA1,
        SHA256,
        SHA512
    }

    public static class Crypto
    {
        public static string GetHash(string data, HashType hashType)
        {

            var bytes = Encoding.Default.GetBytes(data);
            HashAlgorithm hashAlgorithm = null;
            switch (hashType)
            {
                case HashType.MD5:
                    hashAlgorithm = new MD5CryptoServiceProvider();
                    break;
                case HashType.SHA1:
                    hashAlgorithm = new SHA1CryptoServiceProvider();
                    break;
                case HashType.SHA256:
                    hashAlgorithm = new SHA256CryptoServiceProvider();
                    break;
                case HashType.SHA512:
                    hashAlgorithm = new SHA512CryptoServiceProvider();
                    break;
            }

            var hash = hashAlgorithm.ComputeHash(bytes);
            var sb = new StringBuilder(hash.Length * 2);

            foreach (byte b in hash)
            {
                // can be "X2" if you want uppercase
                //
                sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }
    }
}
