﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace HACCP.Admin.Infrastructure
{
    public static class XML
    {
        public static string Serialize<T>(T entity, XmlAttributeOverrides xmlOverrides = null) where T : class
        {
            var settings = new XmlWriterSettings()
            {
                OmitXmlDeclaration = true,
                Indent = true
            };

            var xmlSerializer = xmlOverrides == null
                ? new XmlSerializer(typeof(T))
                : new XmlSerializer(typeof(T), xmlOverrides);

            using (var stringWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(stringWriter, settings))
                {
                    xmlSerializer.Serialize(xmlWriter, entity);
                    return stringWriter.ToString();
                }
            }
        }

        public static T Deserialize<T>(string xmlValue, XmlAttributeOverrides xmlOverrides = null) where T : class
        {
            var serializer = xmlOverrides == null
                ? new XmlSerializer(typeof(T))
                : new XmlSerializer(typeof(T), xmlOverrides);

            using (var reader = new StringReader(xmlValue))
            {
                return (T)serializer.Deserialize(reader);
            };
        }
    }
}
