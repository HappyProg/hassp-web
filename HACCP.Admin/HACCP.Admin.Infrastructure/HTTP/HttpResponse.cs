﻿namespace HACCP.Admin.Infrastructure
{
    public class HttpResponse
    {
        public string Data { get; set; }
        public int StatusCode { get; set; }
        public bool IsSuccessStatusCode { get; set; }
    }
}
