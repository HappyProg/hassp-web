﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HACCP.Admin.Infrastructure
{
    public class HttpBuilder
    {
        private readonly HttpClient _httpClient;        
        private readonly string _url;
        private string _body;

        public HttpBuilder(string url)
        {
            _url = url;
            _httpClient = new HttpClient();
            _httpClient.Timeout = TimeSpan.FromSeconds(30);
        }

        public static HttpBuilder Create(string url)
        {
            return new HttpBuilder(url);
        }

        public HttpBuilder SetBody(string val)
        {
            _body = val;
            return this;
        }

        public HttpBuilder SetTimeout(int val)
        {
            _httpClient.Timeout = TimeSpan.FromSeconds(val);
            return this;
        }

        public HttpBuilder SetBasicAuthorization(string username, string password)
        {
            TryRemoveHeader("Authorization");
             var authVal = "Basic " + Convert.ToBase64String(Encoding
                .GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));

            _httpClient.DefaultRequestHeaders.Add("Authorization", authVal);

            return this;
        }

        public HttpBuilder SetHeader(string headerName, string value)
        {
            TryRemoveHeader(headerName);
            _httpClient.DefaultRequestHeaders.Add(headerName, value);

            return this;
        }

        public async Task<HttpResponse> PostAsync()
        {
            var content = new StringContent(_body);
            var response = await _httpClient.PostAsync(_url, content);
            return await GetResult(response);
        }

        public async Task<HttpResponse> GetAsync()
        {             
            var response = await _httpClient.GetAsync(_url);
            return await GetResult(response);
        }

        private void TryRemoveHeader(string headerName)
        {
            var headAuth = _httpClient.DefaultRequestHeaders.FirstOrDefault(x => x.Key.ToUpper() == headerName.ToUpper());
            if (string.IsNullOrWhiteSpace(headAuth.Key) == false)
            {
                _httpClient.DefaultRequestHeaders.Remove(headAuth.Key);
            }
        }

        private async Task<HttpResponse> GetResult(HttpResponseMessage response)
        {
            var data = await response
                .Content
                .ReadAsStringAsync();

            return new HttpResponse
            {
                Data = data,
                IsSuccessStatusCode = response.IsSuccessStatusCode,
                StatusCode = (int)response.StatusCode
            };
        }
    }
}
