﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace HACCP.Admin.Infrastructure
{
    public static class JWTManager
    {
        public static SymmetricSecurityKey SecretKey => new SymmetricSecurityKey(Encoding.UTF8.GetBytes("48C1D1FDDD3D4AC791AEB7908D7CAEB5E561A395794E4E2EB2B7369ACBA52116"));
        public static JwtTokenData CreateToken(IEnumerable<Claim> claims)
        {
            var signinCredentials = new SigningCredentials(SecretKey, SecurityAlgorithms.HmacSha256);
            var dateExpired = DateTime.Now.AddDays(7);
            var tokeOptions = new JwtSecurityToken(
                issuer: "ufsin.claims",
                audience: "ufsin.claims",
                claims: claims,
                expires: dateExpired,
                signingCredentials: signinCredentials
            );

            return new JwtTokenData
            {
                Token = new JwtSecurityTokenHandler().WriteToken(tokeOptions),
                DateExpired = dateExpired
            };
        }
    }
}
