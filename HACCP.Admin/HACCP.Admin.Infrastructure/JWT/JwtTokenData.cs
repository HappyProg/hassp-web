﻿using System;

namespace HACCP.Admin.Infrastructure
{
    public class JwtTokenData
    {
        public string Token { get; set; }
        public DateTime DateExpired { get; set; }
    }
}
