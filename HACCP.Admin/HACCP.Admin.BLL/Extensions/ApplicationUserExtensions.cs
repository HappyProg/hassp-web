﻿using HACCP.Admin.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace HACCP.Admin.BLL
{
    public static class ApplicationUserExtensions
    {
        public static bool IsEmailValid(this IApplicationUser user)
        {
            if (user is null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            var attr = new EmailAddressAttribute();
            return attr.IsValid(user.Email);
        }
    }
}
