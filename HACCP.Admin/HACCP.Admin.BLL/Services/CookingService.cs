﻿using AutoMapper;
using HACCP.Admin.DAL;
using HACCP.Admin.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACCP.Admin.BLL
{
    public class CookingService : BaseDataService, ICookingService
    {
        public CookingService(ApplicationDbContext database, IMapper mapper) : base(mapper, database)
        {
        }

        //public async Task<ICookingAct> Create(ICookingAct cookingAct)
        //{
        //    if (cookingAct is null)
        //    {
        //        throw new ArgumentNullException(nameof(cookingAct));
        //    }


        //}

        public async Task<ICookingAct> Create(IRecipeCard recipeCard, decimal amount, IStore store)
        {
            if (amount <= 0)
            {
                throw new InvalidOperationException("Укажите выход партии приготовления (в базовых единицах измерения)!");
            }

            if (recipeCard == null)
            {
                throw new InvalidOperationException("Не указана тех. карта!");
            }

            if (store == null)
            {
                throw new InvalidOperationException("Не указан склад списания!");
            }

            var currentDate = DateTime.Now;
            var recipeEntity = await _database.RecipeCards.FirstOrDefaultAsync(x => x.Id == recipeCard.Id);

            if (recipeEntity == null)
            {
                recipeEntity = _mapper.Map<RecipeCard>(recipeCard);
                FillServiceFields(recipeEntity);
                foreach(var i in recipeEntity.Items)
                {
                    FillServiceFields(i);
                }

                await _database.AddAsync(recipeEntity);
            }

            var cookingAct = new CookingAct
            {
                Date = currentDate,
                Id = Guid.NewGuid(),
                DateCreated = currentDate,
                RecipeCard = recipeEntity,
                ResultAmount = amount,
                DateModified = currentDate,
            };

            // Новая партия - результат готовки
            //
            var accumResultBatch = await CreateResultBatch(cookingAct, store);

            // Списываем партии (создаем движения для документа)
            //
            var substractedBatches = Substract(cookingAct, store);

            // Установка зависимоестей движения результирующей партии от списываемых
            //
            foreach (var subBatch in substractedBatches)
            {
                // Добавляем связь новой партии со списанной
                //
                await _database.AccumBatchRelations.AddAsync(new AccumBatchRelation
                {
                    AccumBatchFrom = subBatch,
                    AccumBatchTo = accumResultBatch
                });
            }

            await _database.CookingActs.AddAsync(cookingAct);
            await _database.SaveChangesAsync();

            return _mapper.Map<ICookingAct>(cookingAct);
        }

        #region private

        private async Task<AccumRegBatch> CreateResultBatch(CookingAct cookingAct, IStore store)
        {
            var currentDate = DateTime.Now;
            var recipeCard = cookingAct.RecipeCard;
            var amount = cookingAct.ResultAmount;

            var um = await _database.UnitMeasurements.FirstAsync(x => x.Id == cookingAct.UnitMeasurementId);
            var resProduct = await _database.Products.FirstAsync(x => x.Id == recipeCard.ResultProductId);
            var storeEntity = await _database.Stores.FirstAsync(x => x.Id == store.Id);

            // Новая партия
            //            
            var resultBatch = await BatchBuilder.Create(_database)
                    .SetAmount(amount)
                    .SetProduct(resProduct)
                    .SetStore(storeEntity)
                    .SetUnitMeasurement(um)
                    .SetTypeCreater(OperationType.Cooking)
                    .Build();

            // Новая запись в регистре
            //
            var accumResultBatch = new AccumRegBatch
            {
                Amount = amount,
                Date = currentDate,
                Batch = resultBatch,
                Document = cookingAct,
                OperationType = OperationType.Cooking
            };

            await _database.Batches.AddAsync(resultBatch);
            await _database.AccumRegBatches.AddAsync(accumResultBatch);

            cookingAct.ResultBatch = resultBatch;
            return accumResultBatch;
        }

        private IEnumerable<AccumRegBatch> Substract(CookingAct cookingAct, IStore store)
        {
            // Результат - все движения списания
            //
            var substractedBatches = new List<AccumRegBatch>();

            var recipeCard = cookingAct.RecipeCard;
            var amount = cookingAct.ResultAmount;

            // Кэшируем все необходимые партии по необходимому кол-ву, складу и товарам
            //
            var recipeProductIds = recipeCard.Items.Select(s => s.Product.Id);
            var cashBatches = _database.Batches
                .AsNoTracking()
                .Where(x => x.CurrentAmount > 0 && x.Store.Id == store.Id && recipeProductIds.Contains(x.Product.Id))
                .OrderBy(o => o.Date);

            // Идем по позициям тех. карты
            //
            foreach (var i in recipeCard.Items)
            {
                var totalAmount = i.AmountIn * amount;
                var itemBatches = cashBatches.Where(x => x.Product.Id == i.Product.Id);

                // Проверяем хвататет ли у нас на балансе кол-ва во всех партиях для данного товара
                //
                var balanceAmount = itemBatches.Sum(s => s.CurrentAmount);

                if (balanceAmount < totalAmount)
                {
                    throw new InvalidOperationException($"Нет необходимого количества продукта \"{i.Product.Name}\" на " +
                        $"остатках для склада \"{store.Name}\". На остатках - {balanceAmount},  требуется {totalAmount}, {cookingAct.UnitMeasurement.Name}");
                }

                // Списываем партии пока не получим необходимое кол-во результирущеей партии
                //                
                foreach (var b in itemBatches)
                {
                    // Списываем необходимое кол-во из партии, но не более допустимого - CurrentAmount
                    //
                    var substractAmount = Math.Min(b.CurrentAmount, totalAmount);
                    var sBatch = SubstractBatch(b, substractAmount, cookingAct);
                    substractedBatches.Add(sBatch);

                    if (totalAmount <= substractAmount)
                    {
                        break;
                    }

                    totalAmount -= substractAmount;
                }
            }

            return substractedBatches;
        }

        private AccumRegBatch SubstractBatch(Batch batch, decimal amount, CookingAct ca)
        {
            var currentDate = DateTime.Now;

            // Запись списания партии в реестре
            //
            var accumReg = new AccumRegBatch
            {
                Batch = batch,
                Date = currentDate,
                Document = ca,
                OperationType = OperationType.Cooking,
                Amount = amount * -1
            };

            // Последняя запись в реестре по FIFO
            //
            var accumRegParent = _database.AccumRegBatches
                .OrderBy(o => o.Id)
                .First(x => x.BatchId == batch.Id && x.Amount > 0);

            // Обновляем текущее кол-во в самой партии
            //
            accumReg.Batch.CurrentAmount += amount * -1;

            // Указываем связь партии FROM -> TO
            //
            _database.AccumBatchRelations.Add(new AccumBatchRelation
            {
                AccumBatchFrom = accumRegParent,
                AccumBatchTo = accumReg
            });
            _database.AccumRegBatches.Add(accumReg);

            return accumReg;
        }

        #endregion
    }
}
