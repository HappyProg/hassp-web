﻿using AutoMapper;
using HACCP.Admin.DAL;

namespace HACCP.Admin.BLL
{
    internal class BatchService : IBatchService
    {
        private readonly ApplicationDbContext _database;
        private readonly IMapper _mapper;

        public BatchService(ApplicationDbContext database, IMapper mapper)
        {
            _database = database;
            _mapper = mapper;
        }

        public void Test()
        {

        }

        //public async Task<IConsignment> Create(IDocumentProductItem item, IDocument parentDoc)
        //{
        //    var cons = new Consignment
        //    {
        //        Id = Guid.NewGuid(),
        //        Quantity = item.Quantity,
        //        CurrentQuantity = item.Quantity,
        //        Date = parentDoc.Date                
        //    };

        //    await _database.Consignments.AddAsync(cons);

        //    var accumReg = new AccumRegConsignment
        //    {
        //        Id = Guid.NewGuid(),
        //        Quantity = item.Quantity,
        //        Consignment = cons,
        //        OperationType = OperationType.Inc,
        //        ParentDoc = parentDoc
        //    };
        //}
    }
}
