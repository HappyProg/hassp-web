﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using HACCP.Admin.DAL;
using HACCP.Admin.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace HACCP.Admin.BLL
{
    public class ProductService : BaseRefService, IProductService
    {
        public ProductService(IMapper mapper, ApplicationDbContext database) : base(mapper, database)
        {
        }

        public Task<IEnumerable<IProduct>> SaveAsync(IEnumerable<IProduct> products)
        {
            return SaveByIdAsync<IProduct, Product>(products);
        }

        public Task SaveAsync(IProduct product)
        {
            return SaveByIdAsync<IProduct, Product>(product);
        }

        public async Task<IEnumerable<IProduct>> GetAsync()
        {
            var result = await Database.Products                
                .AsNoTracking()
                .ProjectTo<IProduct>(_mapper.ConfigurationProvider)
                .Take(500)
                .ToArrayAsync();

            return result;
        }

        //private async Task CheckPrefixes(IEnumerable<IProduct> products)
        //{
        //    var ids = products.Select(s => s.Id).ToArray();
        //    var notPrefixProducts = await _database.Products
        //        .Where(x => ids.Contains(x.Id) && string.IsNullOrWhiteSpace(x.Prefix))
        //        .ToArrayAsync();
        //    var existPrefixes = await _database.Products
        //        .Select(s => s.Prefix)
        //        .Distinct()
        //        .ToArrayAsync();

        //    foreach (var p in notPrefixProducts)
        //    {
        //        var prefix = p.Name.Transliterate(3);

        //    }
        //}

        //private string GenerateNewPrefix(IEnumerable<string> exist, string currentPrefix, int currentIndex = 0)
        //{
        //    if (exist.Any(x => x.ToUpper() == currentPrefix))
        //    {
        //        var existPrefix = exist
        //            .OrderByDescending(o => o)
        //            .First(x => x.ToUpper().Contains(currentPrefix));

        //        var newPrefix = 
        //        GenerateNewPrefix(exist, )
        //    }
        //}

        //private string IncrementPrefixIndex(string oldPrefix, string newPrefix)
        //{
        //    if (oldPrefix.Length == newPrefix.Length)
        //    {
        //        return newPrefix + "1";
        //    }
        //    else
        //    {
        //        var index = oldPrefix.Substring(0, newPrefix.Length);
        //    }
        //}
    }
}
