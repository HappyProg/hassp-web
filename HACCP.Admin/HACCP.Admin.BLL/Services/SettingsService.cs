﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using HACCP.Admin.DAL;
using HACCP.Admin.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace HACCP.Admin.BLL
{
    public class SettingsService : BaseRefService, ISettingsService
    {
        public SettingsService(IMapper mapper, ApplicationDbContext database) : base(mapper, database)
        {
        }

        public async Task<IApplicationSettings> GetAsync()
        {
            var settings = await _database.Settings
                .ProjectTo<IApplicationSettings>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            return settings;
        }

        public async Task SaveAsync(IApplicationSettings dto)
        {
            if (dto is null)
            {
                throw new System.ArgumentNullException(nameof(dto));
            }

            var e = await _database.Settings.FirstOrDefaultAsync(x => x.Id == dto.Id);
            if (e == null)
            {
                e = _mapper.Map<ApplicationSettings>(dto);
                e.DateCreated = DateTime.Now;
                e.DateModified = DateTime.Now;
                if (e.Id == Guid.Empty)
                    e.Id = Guid.NewGuid();

                await _database.AddAsync(e);
            }
            else
            {
                _mapper.Map(dto, e);
            }

            await _database.SaveChangesAsync();
        }
    }
}
