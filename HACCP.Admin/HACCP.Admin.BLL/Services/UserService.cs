﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using HACCP.Admin.DAL;
using HACCP.Admin.Infrastructure;
using HACCP.Admin.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HACCP.Admin.BLL
{
    public class UserService : BaseRefService, IUserService
    {
        private readonly SignInManager<ApplicationUser> _signInManager;

        public UserService(IMapper mapper, ApplicationDbContext database, SignInManager<ApplicationUser> signInManager) : base(mapper, database)
        {
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
        }

        public async Task<IApplicationUser> AuthenticateAsync(string username, string password)
        {
            var result = await _signInManager.PasswordSignInAsync(username, password, false, false);

            if (result.Succeeded == false)
            {
                throw new UnauthorizedAccessException("Ошибка авторизации! Проверьте имя пользователя и пароль...");
            }

            var userEntity = await _signInManager.UserManager.FindByNameAsync(username);
            if (userEntity == null)
            {
                throw new UnauthorizedAccessException("Ошибка авторизации! Проверьте имя пользователя и пароль...");
            }


            var claims = new Claim[]
            {
                new Claim(ClaimTypes.Name, username),
                new Claim(ClaimTypes.Email, userEntity.Email)
            };

            var userDTO = _mapper.Map<IApplicationUser>(userEntity);
            var token = JWTManager.CreateToken(claims);
            userDTO.PasswordSalt = token.Token;
            userDTO.TokenExpired = token.DateExpired;

            return userDTO;
        }

        public async Task<IApplicationUser> CreateAsync(IApplicationUser user)
        {
            if (user is null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (string.IsNullOrWhiteSpace(user.Password) || string.IsNullOrWhiteSpace(user.Username))
                throw new InvalidOperationException("Должны быть заданы имя пользователя и пароль!");

            if (user.IsEmailValid() == false)
                throw new InvalidOperationException("Некорректный Email!");

            var checkUserEmail = await _database.Users.FirstOrDefaultAsync(x => x.Email.ToLower() == user.Email.ToLower());

            if (checkUserEmail != null)
                throw new InvalidOperationException("Пользователь с таким email уже существует!");

            var userEntity = _mapper.Map<ApplicationUser>(user);
            userEntity.Id = Guid.NewGuid();

            // Добавляем пользователя
            //
            var result = await _signInManager.UserManager.CreateAsync(userEntity, user.Password);
            if (result.Succeeded == false)
            {
                var errors = result.Errors
                    .Select(s => $"{s.Code}: {s.Description}")
                    .ToArray();
                throw new InvalidOperationException(string.Join("\r\n", errors));
            }

            var userLogged = await AuthenticateAsync(user.Username, user.Password);

            return userLogged;
        }

        public async Task<IApplicationUser> GetAsync(Guid id)
        {
            var result = await _database
                .Users
                .AsNoTracking()
                .ProjectTo<IApplicationUser>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(x => x.Id == id);

            return result;
        }

        public async Task<IEnumerable<IApplicationUser>> GetAsync()
        {
            var result = await _database.Users
                .ProjectTo<IApplicationUser>(_mapper.ConfigurationProvider)
                .AsNoTracking()
                .ToListAsync();

            return result;
        }

        public async Task<IApplicationUser> GetByNameAsync(string username)
        {
            var user = await _database.Users
                .ProjectTo<IApplicationUser>(_mapper.ConfigurationProvider)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Username == username);

            if (user == null)
            {
                throw new InvalidOperationException("Пользоватаель не найден!");
            }

            return user;
        }

        public async Task<IEnumerable<IApplicationUser>> FindAsync(string searchStr = null)
        {
            var result = string.IsNullOrWhiteSpace(searchStr)
                ? await _database.Users
                    .AsNoTracking()
                    .ToListAsync()
                : await _database.Users
                    .AsNoTracking()
                    .Where(x => x.UserName.Contains(searchStr)
                        || x.Email.Contains(searchStr) || x.FirstName.Contains(searchStr)
                        || x.LastName.Contains(searchStr))
                    .AsNoTracking()
                    .ToListAsync();

            return _mapper.Map<IEnumerable<IApplicationUser>>(result);
        }
    }
}
