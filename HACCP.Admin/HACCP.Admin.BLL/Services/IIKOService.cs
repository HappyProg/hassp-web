﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using HACCP.Admin.DAL;
using HACCP.Admin.Interfaces;
using HACCP.IIKO.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACCP.Admin.BLL
{
    public class IIKOService : BaseRefService, IExternalImportService
    {       
        private static class IIKOConst
        {
            public const string OLAP_COOKING_ACT_NAME = "Акт приготовления";
        }

        private readonly ISettingsService _settingsService;

        private async Task<IikoAPIService> InitApiServiceAsync()
        {
            var settings = await _settingsService.GetAsync();
            var host = settings.IikoURL;
            var username = settings.IikoUsername;
            var password = settings.IikoPassword;
            return new IikoAPIService(host, username, password);
        }

        public IIKOService(IMapper mapper, ApplicationDbContext database, ISettingsService settingsService) : base(mapper, database)
        {
            _settingsService = settingsService ?? throw new ArgumentNullException(nameof(settingsService));            
        }

        public async Task<IEnumerable<IInvoice>> GetInvoicesAsync(DateTime dateFrom, DateTime dateTo)
        {
            var apiService = await InitApiServiceAsync();
            var result = await apiService.GetInvoicesAsync(dateFrom, dateTo);
            return _mapper.Map<IEnumerable<IInvoice>>(result);
        }

        public async Task<IEnumerable<IRecipeCard>> GetRecipeCardsAsync(DateTime dateFrom, DateTime dateTo, int revision = 0)
        {
            var apiService = await InitApiServiceAsync();
            var iikoResult = await apiService.GetRecipeCardsAsync(dateFrom, dateTo, revision);
            var result = _mapper.Map<IEnumerable<IRecipeCard>>(iikoResult.AssemblyCharts);

            foreach(var dto in result)
            {
                dto.Revision = iikoResult.KnownRevision;
            }

            return result;
        }


        public async Task<IEnumerable<IProduct>> GetProductCategoriesAsync()
        {
            var apiService = await InitApiServiceAsync();
            var result = await apiService.GetProductCategoriesAsync();
            return _mapper.Map<IEnumerable<IProduct>>(result);
        }

        public async Task<IEnumerable<IProduct>> GetProductsAsync()
        {
            var apiService = await InitApiServiceAsync();
            var result = await apiService.GetProductsAsync();
            return _mapper.Map<IEnumerable<IProduct>>(result);
        }

        public async Task<IEnumerable<ICookingAct>> GetCookingActs(DateTime dateFrom, DateTime dateTo)
        {
            var apiService = await InitApiServiceAsync();
            var cookingActsIiko = await apiService.GetCookingActs(dateFrom, dateTo);
            // Единицы измерения
            //
            var units = cookingActsIiko
                .SelectMany(s => s.Items.Products.Select(p => p.UnitMeasureName))
                .Distinct()
                .Select(s => new UnitMeasurementDTO
                {
                    Name = s
                }).AsEnumerable<IUnitMeasurement>();

            // Склады
            //
            var stores = cookingActsIiko
                .SelectMany(s => s.Items.Products.Select(p => p.StoreName))
                .Distinct()
                .Select(s => new StoreDTO
                {
                    Name = s
                })
                .AsEnumerable<IStore>();
            units = await SaveByNameAsync<IUnitMeasurement, UnitMeasurement>(units);
            stores = await SaveByNameAsync<IStore, Store>(stores);

            var products = cookingActsIiko
                .SelectMany(s => s.Items.Products.Select(p => p));
            // Приготовленные продукты
            //
            var resultProducts = products
                .Where(x => x.Amount > 0).ToArray();

            var resultProductsIds = resultProducts.Select(s => s.ProductId);

            //var recipes = _database.RecipeCards
            //    .ProjectTo<IRecipeCard>(_mapper.ConfigurationProvider)
            //    .Where(x => resultProductsIds.Contains(x.ResultProductId))
            //    .AsEnumerable();

            //var excludedRecipes = resultProductsIds.Except(recipes.Select(s => s.ResultProductId));
            //if (excludedRecipes.Count() > 0)
            //{
            //    recipes = await GetRecipeCardsAsync(dateFrom, dateTo);
            //}

            var recipes = await GetRecipeCardsAsync(dateFrom, dateTo);


            recipes = recipes.Where(x => resultProducts.Select(s => s.ProductId).Contains(x.ResultProductId));

            var result = cookingActsIiko.SelectMany(doc => 
                doc.Items.Products.Where(x => x.Amount > 0).Select(p => new CookingActDTO
            {
                Date = doc.DateIncoming,
                Number = doc.DocumentNumber,
                RecipeCard = recipes.First(x => x.ResultProductId == p.ProductId),
                ResultAmount = p.Amount,
                StoreId = (Guid)stores.First(x => x.Name == p.StoreName).Id,
                Store = stores.First(x => x.Name == p.StoreName)
                }));

            return result;
        }
    }
}
