﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using HACCP.Admin.DAL;
using HACCP.Admin.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACCP.Admin.BLL
{
    public class InvoiceService : BaseRefService, IInvoiceService
    {
        private readonly IExternalImportService _iikoService;
        private readonly IProductService _productService;

        public InvoiceService(IMapper mapper, ApplicationDbContext database, IExternalImportService iikoService, IProductService productService) : base(mapper, database)
        {
            _iikoService = iikoService ?? throw new ArgumentNullException(nameof(iikoService));
            _productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        public async Task<IInvoice> GetAsync(Guid id)
        {
            var result = await _database.Invoices
                .ProjectTo<IInvoice>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync(x => x.Id == id);

            return result;
        }

        public async Task<IEnumerable<IInvoice>> LoadFromExternalServiceAsync(DateTime from, DateTime to)
        {
            await LoadIIKOProducts();

            var invoices = await _iikoService.GetInvoicesAsync(from, to);
            
            var ids = invoices.SelectMany(s => s.Items.Select(ss => ss.ProductId)).Distinct().ToArray();
            var products = await _database.Products
                .Where(x => ids.Contains(x.Id))
                .ToArrayAsync();

            var store = await _database.Stores.FirstAsync(x => x.Id == Guid.Parse("AAFF793F-9F78-44DF-AF22-FC9C03664D13"));
            var storeDTO = _mapper.Map<IStore>(store);
            var um = await _database.UnitMeasurements.FirstAsync(x => x.Id == Guid.Parse("DDFF793F-9F78-44DF-AF22-FC9C03664D23"));
            var umDTO = _mapper.Map<IUnitMeasurement>(um);
            foreach (var inv in invoices)
            {
                foreach (var i in inv.Items)
                {
                    var p = products.First(x => x.Id == i.ProductId);
                    i.StoreId = storeDTO.Id;
                    i.UnitMeasurementId = umDTO.Id;
                    //i.Product = _mapper.Map<IProduct>(p);
                }

            }

            await SaveAsync(invoices);

            foreach (var inv in invoices)
            {
                var invoice = _database.Invoices.First(x => x.Id == inv.Id);
                foreach(var p in inv.Items)
                {
                    var product = products.First(x => x.Id == p.ProductId);

                    var b = BatchBuilder.Create(_database)
                        .SetAmount(500)
                        .SetDate(DateTime.Now)
                        .SetProduct(product)
                        .SetStore(store)
                        .SetUnitMeasurement(um)
                        .Build().Result;

                    var bReg = new AccumRegBatch
                    {
                        Amount = b.Amount,
                        Date = DateTime.Now,
                        Batch = b,
                        Document = invoice,
                        OperationType = OperationType.Inc
                    };

                    _database.Batches.Add(b);
                    _database.AccumRegBatches.Add(bReg);

                }
            }

            await _database.SaveChangesAsync();

            return invoices;

        }

        public Task<IEnumerable<IInvoice>> GetAsync()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IInvoice>> SaveAsync(IEnumerable<IInvoice> inv)
        {
            //return SaveAsync<IInvoice, Invoice>(inv);
            return null;
        }

        public Task SaveAsync(IInvoice inv)
        {
            //return SaveAsync<IInvoice, Invoice>(inv);
            return null;
        }

        private async Task<IEnumerable<IProduct>> LoadIIKOProducts()
        {
            var c = await _iikoService.GetProductCategoriesAsync();
            c = await _productService.SaveAsync(c);
            var p = await _iikoService.GetProductsAsync();
            p = await _productService.SaveAsync(p);

            return p;
        }
    }
}
