﻿using AutoMapper;
using HACCP.Admin.DAL;
using HACCP.Admin.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HACCP.Admin.BLL
{
    public class RecipeCardService : BaseDataService, IRecipeCardService
    {
        public RecipeCardService(IMapper mapper, ApplicationDbContext database) : base(mapper, database)
        {
        }

        public async Task<IRecipeCard> CreateAsync(IRecipeCard card)
        {
            if (card is null)
            {
                throw new System.ArgumentNullException(nameof(card));
            }

            var entity = _mapper.Map<RecipeCard>(card);
            FillServiceFields(entity);
            foreach (var i in entity.Items)
            {
                FillServiceFields(i);
            }

            await _database.AddAsync(entity);
            await _database.SaveChangesAsync();

            return _mapper.Map<IRecipeCard>(entity);
        }

        public async Task<IEnumerable<IRecipeCard>> CreateAsync(IEnumerable<IRecipeCard> cards)
        {
            if (cards is null)
            {
                throw new System.ArgumentNullException(nameof(cards));
            }

            var entities = _mapper.Map<IEnumerable<RecipeCard>>(cards);

            foreach (var e in entities)
            {
                FillServiceFields(e);
                foreach (var i in e.Items)
                {
                    FillServiceFields(i);
                }
            }

            await _database.AddRangeAsync(entities);
            await _database.SaveChangesAsync();

            return _mapper.Map<IEnumerable<IRecipeCard>>(entities);
        }
    }
}
