﻿using AutoMapper;
using HACCP.Admin.DAL;
using HACCP.Admin.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HACCP.Admin.BLL
{
    public abstract class BaseDataService : BaseService
    {
        protected readonly ApplicationDbContext _database;
        public ApplicationDbContext Database => _database;

        public BaseDataService(IMapper mapper, ApplicationDbContext database) : base(mapper)
        {
            _database = database ?? throw new ArgumentNullException(nameof(database));
        }

        /// <summary>
        /// Заполнение служебных полей (Id, DateCreated и т.д.)
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="isNew"></param>
        protected void FillServiceFields<TEntity>(TEntity entity, bool isNew = true)
            where TEntity : BaseEntity
        {
            if (entity.Id == null || entity.Id == Guid.Empty)
            {
                entity.Id = Guid.NewGuid();
            }

            var currentDate = DateTime.Now;
            entity.DateModified = currentDate;

            if (isNew)
            {
                entity.DateCreated = currentDate;
            }
        }

        protected async Task<TEntity> CheckInsertUpdate<TEntity>(IBaseRefEntity source) where TEntity : BaseRefEntity
        {
            var dbSet = _database.Set<TEntity>();
            var entity = await dbSet.FirstOrDefaultAsync(x => x.Id == source.Id);

            if (entity == null)
            {
                entity = _mapper.Map<TEntity>(source);
                FillServiceFields(entity);

                await dbSet.AddAsync(entity);
            }
            else
            {
                entity = _mapper.Map(source, entity);
                FillServiceFields(entity);
                dbSet.Update(entity);
            }

            return entity;
        }

        protected async Task<IEnumerable<TEntity>> Insert<TEntity, TEntityDTO>(IEnumerable<TEntityDTO> source)
            where TEntity : BaseRefEntity
            where TEntityDTO : IBaseRefEntity
        {
            var dbSet = _database.Set<TEntity>();
            var dest = _mapper.Map<IEnumerable<TEntity>>(source);
            foreach (var e in dest)
            {
                FillServiceFields(e);
            }

            await dbSet.AddRangeAsync(dest);
            return dest;
        }

        protected TEntity UpdateEntity<TEntity>(IBaseRefEntity source, TEntity dest) where TEntity : BaseRefEntity
        {
            var dbSet = _database.Set<TEntity>();
            dest = _mapper.Map(source, dest);
            FillServiceFields(dest, false);
            dbSet.Update(dest);

            return dest;
        }

        protected async Task<TEntity> Insert<TEntity, TEntityDTO>(TEntityDTO source)
            where TEntity : BaseRefEntity
            where TEntityDTO : IBaseRefEntity
        {
            var dbSet = _database.Set<TEntity>();
            var dest = _mapper.Map<TEntity>(source);
            FillServiceFields(dest);

            await dbSet.AddAsync(dest);
            return dest;
        }

        protected async Task<IEnumerable<TEntityDTO>> AddAsync<TEntityDTO, TEntity>(IEnumerable<TEntityDTO> dto)
            where TEntity : BaseRefEntity
            where TEntityDTO : IBaseRefEntity
        {
            var addedEntities = await Insert<TEntity, TEntityDTO>(dto);
            await _database.SaveChangesAsync();

            return _mapper.Map(addedEntities, dto);
        }
    }
}
