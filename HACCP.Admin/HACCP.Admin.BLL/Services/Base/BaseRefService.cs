﻿using AutoMapper;
using HACCP.Admin.DAL;
using HACCP.Admin.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACCP.Admin.BLL
{
    public abstract class BaseRefService : BaseDataService
    {
        public BaseRefService(IMapper mapper, ApplicationDbContext database) : base(mapper, database)
        {
        }

        protected async Task<TEntityDTO> SaveByIdAsync<TEntityDTO, TEntity>(IBaseRefEntity source) 
            where TEntity : BaseRefEntity
            where TEntityDTO : IBaseRefEntity
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            var entity = await CheckInsertUpdate<TEntity>(source);
            await _database.SaveChangesAsync();

            return _mapper.Map<TEntityDTO>(entity);
        }

        protected async Task<IEnumerable<TEntityDTO>> SaveByIdAsync<TEntityDTO, TEntity>(IEnumerable<TEntityDTO> dtoCollection)
            where TEntity : BaseRefEntity
            where TEntityDTO : IBaseRefEntity
        {
            if (dtoCollection is null)
            {
                throw new ArgumentNullException(nameof(dtoCollection));
            }
            
            var entities = await GetEntitiesForUpdate<TEntityDTO, TEntity, Guid?>(dtoCollection, dto => dto.Id, e => e.Id);
            var entitiesToUpdate = entities.ToDictionary(e => dtoCollection.First(x => x.Id == e.Id), e => e);
            // Все оставшиеся эелементы доабвляем в базу как новые
            //
            var entitiesToAdd = dtoCollection.Where(x => entities.Select(s => s.Id as Guid?).Contains(x.Id) == false)
                .ToArray();

            var result = await SaveAsync(entitiesToUpdate, entitiesToAdd);
            return result;
        }

        protected async Task<IEnumerable<TEntityDTO>> SaveByNameAsync<TEntityDTO, TEntity>(IEnumerable<TEntityDTO> dtoCollection)
            where TEntity : BaseRefEntity
            where TEntityDTO : IBaseRefEntity
        {
            if (dtoCollection is null)
            {
                throw new ArgumentNullException(nameof(dtoCollection));
            }


            var entities = await GetEntitiesForUpdate<TEntityDTO, TEntity, string>(dtoCollection, dto => dto.Name, e => e.Name);
            var entitiesToUpdate = entities.ToDictionary(e => dtoCollection.First(x => x.Name == e.Name), e => e);
            // Все оставшиеся эелементы доабвляем в базу как новые
            //
            var entitiesToAdd = dtoCollection.Where(x => entities.Select(s => s.Name).Contains(x.Name) == false)
                .ToArray();

            var result = await SaveAsync(entitiesToUpdate, entitiesToAdd);
            return result;
        }

        private async Task<IEnumerable<TEntity>> GetEntitiesForUpdate<TEntityDTO, TEntity, TValue>(IEnumerable<TEntityDTO> dto,
            Func<TEntityDTO, TValue> dtoSelector, Func<TEntity, TValue> entityProperty)
            where TEntity : BaseRefEntity
            where TEntityDTO : IBaseRefEntity
        {
            var dbSet = _database.Set<TEntity>();
            var Ids = dto.Select(dtoSelector).ToArray();

            var entities = dbSet
                   .WhereContains(Ids, entityProperty)
                   .ToArray();

            return entities;
        }

        private async Task<IEnumerable<TEntityDTO>> SaveAsync<TEntityDTO, TEntity>(Dictionary<TEntityDTO, TEntity> entitiesToUpdate, IEnumerable<TEntityDTO> entitiesToAdd)
            where TEntity : BaseRefEntity
            where TEntityDTO : IBaseRefEntity
        {
            var resultEntities = new List<TEntityDTO>();

            foreach (var set in entitiesToUpdate)
            {
                var entity = set.Value;
                var dto = set.Key;

                if (entity is IEqutableDTO<TEntityDTO>)
                {
                    if ((entity as IEqutableDTO<TEntityDTO>).Equals(dto) == false)
                    {
                        entity = UpdateEntity(dto, entity);
                    }
                }
                else
                {
                    entity = await CheckInsertUpdate<TEntity>(dto);                    
                }

                resultEntities.Add(_mapper.Map<TEntityDTO>(entity));
            }

            var added = await AddAsync<TEntityDTO, TEntity>(entitiesToAdd);
            resultEntities.AddRange(added);
            return resultEntities;
        }
    }
}
