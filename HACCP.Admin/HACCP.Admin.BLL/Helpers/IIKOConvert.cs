﻿using HACCP.Admin.Interfaces;

namespace HACCP.Admin.BLL
{
    internal static class IIKOConvert
    {
        public static ProductType ConvertProductType(string type)
        {
            switch (type.ToUpper())
            {
                case "GOODS":
                    return ProductType.Goods;
                case "DISH":
                    return ProductType.Dish;
                case "MODIFIER":
                    return ProductType.Modifier;
                case "PREPARED":
                    return ProductType.Prepared;
                case "SERVICE":
                    return ProductType.Service;
                default:
                    return ProductType.NoSet;
            }
        }

        public static InvoiceStatus ConvertInvoiceStatus(string type)
        {
            switch (type.ToUpper())
            {
                case "NEW":
                    return InvoiceStatus.New;
                case "PROCESSED":
                    return InvoiceStatus.Processed;
                case "DELETED":
                    return InvoiceStatus.Deleted;
                default:
                    return InvoiceStatus.NoSet;
            }
        }
    }
}
