﻿using AutoMapper;
using HACCP.Admin.DAL;
using HACCP.Admin.Interfaces;


namespace HACCP.Admin.BLL
{
    public class MapperProfileDTO : Profile
    {
        public MapperProfileDTO()
        {
            #region settings
            CreateMap<ApplicationSettings, IApplicationSettings>().As<ApplicationSettingsDTO>();
            CreateMap<IApplicationSettings, ApplicationSettings>();
            #endregion

            #region product
            CreateMap<Product, IProduct>().As<ProductDTO>();
            CreateMap<IProduct, Product>();
            #endregion

            #region user
            CreateMap<ApplicationUser, IApplicationUser>().As<ApplicationUserDTO>();
            CreateMap<IApplicationUser, ApplicationUser>();
            CreateMap<ApplicationRole, IApplicationRole>().As<ApplicationRoleDTO>();
            CreateMap<IApplicationRole, ApplicationRole>();
            #endregion

            #region store
            CreateMap<Store, IStore>().As<StoreDTO>();
            CreateMap<IStore, Store>();
            #endregion

            #region unitMeasurement
            CreateMap<UnitMeasurement, IUnitMeasurement>().As<UnitMeasurementDTO>();
            CreateMap<IUnitMeasurement, UnitMeasurement>();
            #endregion

            #region invoice
            CreateMap<Invoice, IInvoice>().As<InvoiceDTO>();
            CreateMap<IInvoice, Invoice>();
            #endregion

            #region recipe card
            CreateMap<RecipeCard, IRecipeCard>().As<RecipeCardDTO>();
            CreateMap<IRecipeCard, RecipeCard>()
                .ForMember(m => m.ResultProduct, opt => opt.Ignore());

            CreateMap<RecipeItem, IRecipeItem>().As<RecipeItemDTO>();
            CreateMap<IRecipeItem, RecipeItem>()
                .ForMember(m => m.Product, opt => opt.Ignore());
            #endregion


            #region document product item
            CreateMap<DocumentProductItem, IDocumentProductItem>().As<DocumentProductItemDTO>();
            CreateMap<IDocumentProductItem, DocumentProductItem>()
                .ForMember(m => m.Store, opt => opt.Ignore())
                .ForMember(m => m.UnitMeasurement, opt => opt.Ignore());
            #endregion
        }
    }
}
