﻿using AutoMapper;
using HACCP.Admin.Interfaces;
using HACCP.IIKO.Infrastructure;
using System;

namespace HACCP.Admin.BLL
{
    public class MapperProfileIIKO : Profile
    {
        public MapperProfileIIKO()
        {
            #region iiko dto
            CreateMap<IikoProductDTO, IProduct>()
                .ForMember(m => m.Comment, opt => opt.MapFrom(s => s.Description))
                .ForMember(m => m.Article, opt => opt.MapFrom(s => s.Num))
                .ForMember(m => m.ProductType, opt => opt.MapFrom(s => IIKOConvert.ConvertProductType(s.Type)))
                .ForMember(m => m.IsDeleted, opt => opt.MapFrom(s => s.Deleted))
                .ForMember(m => m.Parent, opt => opt.Ignore())
                .ForMember(m => m.ParentId, opt => opt.MapFrom(s => s.Parent))
                .As<ProductDTO>();

            CreateMap<IIKOInvoiceDTO, IInvoice>()
                .ForMember(m => m.Date, opt => opt.MapFrom(s => s.IncomingDate))
                .ForMember(m => m.IsDeleted, opt => opt.MapFrom(s => IIKOConvert.ConvertInvoiceStatus(s.Status) == InvoiceStatus.Deleted))
                .ForMember(m => m.Number, opt => opt.MapFrom(s => s.DocumentNumber))
                .ForMember(m => m.Items, opt => opt.MapFrom(s => s.Items.Products))
                .As<InvoiceDTO>();
            CreateMap<IIKOInvoiceItemDTO, IDocumentProductItem>()
                .ForMember(m => m.Id, opt => opt.MapFrom(s => Guid.NewGuid()))
                .ForMember(m => m.Quantity, opt => opt.MapFrom(s => s.ActualAmount))
                .ForMember(m => m.VatAmount, opt => opt.MapFrom(s => s.VatSum))
                .ForMember(m => m.VatPercent, opt => opt.MapFrom(s => s.VatPercent))
                .ForMember(m => m.Product, opt => opt.Ignore())
                .ForMember(m => m.ProductId, opt => opt.MapFrom(s => s.Product))
                .ForMember(m => m.Store, opt => opt.Ignore())
                .ForMember(m => m.StoreId, opt => opt.MapFrom(s => s.Store))
                .ForMember(m => m.UnitMeasurementId, opt => opt.MapFrom(s => s.AmountUnit))
                .As<DocumentProductItemDTO>();

            CreateMap<IIKOAssemblyChart, IRecipeCard>()
                .ForMember(m => m.ResultProductId, opt => opt.MapFrom(s => s.AssembledProductId))
                .ForMember(m => m.Comment, opt => opt.MapFrom(s => s.OutputComment))
                .As<RecipeCardDTO>();

            CreateMap<IIKOAssemblyChartItem, IRecipeItem>()
                .ForMember(m => m.ProductId, opt => opt.MapFrom(s => s.ProductId))
                .As<RecipeItemDTO>();
            #endregion
        }
    }
}
