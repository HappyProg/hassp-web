export class User {
    id : string;
    firstName : string;
    lastName : string;
    email : string;
    password : string;
    passwordSalt : string;
    username : string;
    TokenExpired : Date;

    constructor(username = '', firstName = '', lastName = '', email = ''){
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    fullname() : string {        
        return this.firstName + ' ' + this.lastName;
    }
};