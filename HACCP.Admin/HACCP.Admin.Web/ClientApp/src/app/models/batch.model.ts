export class Batch {
    id : string;
    number : string;
    amount : number;
    product : string;
    unitName : string;

    constructor(){
    }
};