export class Product {
    id : string;
    name : string;
    parentId : string;
    type : number;
    prefix : string;
    code : string;
    article : string;

    constructor(){
    }
};