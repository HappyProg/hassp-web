import { Component, OnInit } from '@angular/core';
import { UserService } from '../Core/Services/user.service';
import { AlertService } from '../Core/Services/alert.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user = new User();
  passwordConfirm : string;
  isProgress = false;
  hidePassword = true;

  constructor(
    private userService : UserService,
    private alertService: AlertService) { }

  ngOnInit(): void {
  }

  login(){
    this.isProgress = true;

    this.userService
      .login(this.user.username, this.user.password)
      .subscribe(
        result => {},
        error => {
          this.alertService.error(error.error);
          this.user.password = '';
      })
      .add(() => this.isProgress = false);
  }

}
