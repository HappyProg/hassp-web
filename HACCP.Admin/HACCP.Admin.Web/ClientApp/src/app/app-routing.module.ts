import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { AuthGuard } from './Core/Services/auth-guard';
import { ProductsComponent } from './products/products.component';
import { ProductInfoComponent } from './product-info/product-info.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { BatchesComponent } from './batches/batches.component';
import { AccumBatchesComponent } from './accum-batches/accum-batches.component';

const childRoutes: Routes = [
    { path: 'main', component: MainMenuComponent },
    { path: 'products', component: ProductsComponent },
    { path: 'batches', component: BatchesComponent },
    { path: 'accum-batches', component: AccumBatchesComponent },
    { path: 'product-info/:id', component: ProductInfoComponent }
];

const routes: Routes = [
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: 'app', component: MainComponent, canActivate: [AuthGuard], children: childRoutes },
  { path: '', redirectTo : 'app/main', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }