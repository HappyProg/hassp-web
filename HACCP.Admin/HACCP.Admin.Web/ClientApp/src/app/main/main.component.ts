import { Component, OnInit } from '@angular/core';
import { UserService } from '../Core/Services/user.service';
import { AlertService } from '../Core/Services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatBottomSheet } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  isExpanded = false;

  navLinks = [
    {
      title : "Главная",
      link : "main",
      icon : "home"
    },
    {
      title : "Партии",
      link : "batches",
      icon : "category"
    },
    {
      title : "Готовка",
      link : "cooking",
      icon : "update"
    },
    {
      title : "Номенклатура",
      link : "products",
      icon : "view_module"
    },
    {
      title : "Отчеты",
      link : "reports",
      icon : "equalizer"
    }            
  ];

  constructor( public userService : UserService,
    private alertService : AlertService,
    private router : Router,
    private route: ActivatedRoute,
    private bottomSheet: MatBottomSheet) { }

  ngOnInit(): void {
  }

  logout(){
    this.userService.logout();
  }

  profileInfo(){
    // this.bottomSheet.open(ProfileInfoComponent);
  }

  notWork(){
    this.alertService.info("Coming soon...")
  }

  showNotifications(){
    // this.bottomSheet.open(NotificationsComponent);
  }

}
