import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserService } from './Services/user.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
      private userService : UserService){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {    
    if (this.userService.isLogged()){
        req = req.clone({
        setHeaders: {
            'Authorization': `Bearer ${this.userService.currentUser.passwordSalt}`,
        },
        });
    }

    return next.handle(req).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status == 401) {
          this.userService.logout();
        }
          return throwError(err);
      })
    );
  }
}