import { ModuleWithProviders, NgModule } from "@angular/core";
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserService } from "./Services/user.service";
import { AlertService } from "./Services/alert.service";
import { AuthInterceptor } from "./auth.interceptor";
import { AuthGuard } from "./Services/auth-guard";
import { ProductService } from "./Services/product.service";


@NgModule({
    imports: [],
    declarations: [],
    providers: []
  })
  export class CoreModule {
    static forRoot(): ModuleWithProviders {
      return {
        ngModule: CoreModule,
        providers: [
          UserService,
          AlertService,
          ProductService,
          AuthGuard,        
          { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }          
        ]
      };
    }
  }