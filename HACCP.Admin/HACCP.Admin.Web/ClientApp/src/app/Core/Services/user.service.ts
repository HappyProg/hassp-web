import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { Consts } from '../app.const';

@Injectable()
export class UserService {
    currentUser : User = new User();

    private storageUser = 'hassp_admin_user'; 

    constructor(
        private http: HttpClient,
        private router: Router,
        private route: ActivatedRoute) { }

    private loginResult(user : User) {
        if (user && user.passwordSalt) {
            this.currentUser = user;
            this.saveUserData(this.currentUser);
            this.redirect();
        }      
    }

    private saveUserData(user : User){
        localStorage.setItem(this.storageUser, JSON.stringify(user));
    }


    isLogged() : boolean {
        let user = JSON.parse(localStorage.getItem(this.storageUser));

        if (user && user.passwordSalt){
            this.currentUser = user;
            return true;
        }
    }

    info() : Observable<User> {
        let url = Consts.API_ENDPOINT + 'api/protected/user-info';
        return this.http.get<User>(url);
    }

    create(user : User){        
        let url = Consts.API_ENDPOINT + 'users';
        return this.http.post<User>(url, user).pipe(
            map((result) => this.loginResult(result))
        );
    }

    // filter(searchString : string) : Observable<UserFiltered[]> {
    //     if (searchString){
    //         let url = AppSettings.API_ENDPOINT + 'api/protected/users/list';
    //         let filter = { filter : searchString };
    //         return this.http.post<UserFiltered[]>(url, filter);
    //     }
    // }

    login(username: string, password: string) {
        let url = Consts.API_ENDPOINT + 'sessions/create';
        let data = { username: username, password: password };
        
        return this.http.post<User>(url, data).pipe(
            map((result) => this.loginResult(result))
        );
    }

    logout() {        
        localStorage.removeItem(this.storageUser);
        this.router.navigate(['/login']);
    }
  
    private redirect(){
        let params = this.route.snapshot.queryParams;
        if (params['redirectURL']) {
            let redirectURL = params['redirectURL'];
            this.router.navigate([redirectURL]);
        }
        else{
            this.router.navigate(['/']);
        }        
    }
}