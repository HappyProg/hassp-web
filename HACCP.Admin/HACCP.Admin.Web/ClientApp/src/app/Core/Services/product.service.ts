import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Product } from "src/app/models/product.model";
import { Consts } from "../app.const";
import { Observable } from "rxjs";

@Injectable()
export class ProductService {
    constructor(
        private http: HttpClient) { } 

        get() : Observable<Product[]>{
            let url = Consts.API_ENDPOINT + 'products';
            return this.http.get<Product[]>(url);   
        }

}