import { Injectable } from '@angular/core';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Injectable()
export class AlertService {
    success(message: string, title: string = 'Успех!') {
        Swal.fire({
            icon: 'success',
            title: title,
            text: message
          })
    }

    error(message: string, title: string = 'Ошибка!') {
        Swal.fire({
            icon: 'error',
            title: title,
            text: message
          })
    }
    
    info(message: string, title: string = 'Информация') {
        Swal.fire({
            icon: 'info',
            title: title,
            text: message
          })
    }
    
    warn(message: string, title: string = 'Внимание!') {
        Swal.fire({
            icon: 'warning',
            title: title,
            text: message
          })
    }

    toastSuccess(message : string){
        this.toast(message, 'success');
    }

    toastError(message : string){
        this.toast(message, 'error');
    }

    toastWarn(message : string){
        this.toast(message, 'warning');
    }

    toastInfo(message : string){
        this.toast(message, 'info');
    }    

    toast(message : string, type: SweetAlertIcon){
        Swal.fire({
            icon: type,
            title: message,
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000                        
          })  
    }
}