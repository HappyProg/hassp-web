import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccumBatchesComponent } from './accum-batches.component';

describe('AccumBatchesComponent', () => {
  let component: AccumBatchesComponent;
  let fixture: ComponentFixture<AccumBatchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccumBatchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccumBatchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
