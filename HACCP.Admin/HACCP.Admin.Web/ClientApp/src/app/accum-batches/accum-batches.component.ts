import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Batch } from '../models/batch.model';
import { MatSort } from '@angular/material/sort';
import { AccumBatch } from '../models/accum-batch.model';

@Component({
  selector: 'app-accum-batches',
  templateUrl: './accum-batches.component.html',
  styleUrls: ['./accum-batches.component.scss']
})
export class AccumBatchesComponent implements OnInit {
  isProgress = false;
  columnsToDisplay = ['number', 'amount', 'unitName', 'product'];
  dataSource : MatTableDataSource<AccumBatch> = new MatTableDataSource<AccumBatch>();
  batches : AccumBatch[]  = [
    {
      accumType : "arrow_upward",
      batch : "GOV-107/05.03",
      amount : 9.00,
      date : "04.03.2020",
      unitName : "кг"
    },
    {
      accumType : "arrow_downward",
      batch : "GOV-107/05.03",
      amount : 2.100,
      date : "04.03.2020",
      unitName : "кг"
    },
    {
      accumType : "arrow_upward",
      batch : "SVN-101/05.03",
      amount : 12.500,
      date : "04.03.2020",
      unitName : "кг"
    },
    {
      accumType : "arrow_downward",
      batch : "SVN-101/05.03",
      amount : 1.300,
      date : "04.03.2020",
      unitName : "кг"
    },
    {
      accumType : "arrow_upward",
      batch : "KTL-107/05.03",
      amount : 1.700,
      date : "05.03.2020",
      unitName : "кг"
    },
    {
      accumType : "arrow_downward",
      batch : "LUK-82/04.03",
      amount : 1.100,
      unitName : "кг",
      date : "04.03.2020"
    },                
  ];

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor() { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.batches);
    this.dataSource.sort = this.sort; 
  }

}
