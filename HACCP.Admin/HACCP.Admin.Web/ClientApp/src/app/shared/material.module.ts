import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatBadgeModule } from '@angular/material/badge';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDividerModule } from '@angular/material/divider';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSortModule } from '@angular/material/sort';
import { NgModule } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatChipsModule } from '@angular/material/chips';

@NgModule({
    imports: [              
    ],
    exports: [
        MatButtonModule,
        MatAutocompleteModule,
        MatCardModule,
        BrowserAnimationsModule,
        MatTabsModule,        
        MatToolbarModule,
        MatIconModule,
        MatMenuModule,
        MatProgressBarModule,
        MatDividerModule,
        MatTooltipModule,
        MatTableModule,
        MatBottomSheetModule,
        MatListModule,
        MatSidenavModule,
        MatBadgeModule,
        MatSortModule,
        MatInputModule,
        MatSelectModule,        
        MatFormFieldModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRippleModule,
        MatChipsModule
    ],
    declarations: [
        
    ]
  })
  export class MaterialModule{}