import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MaterialModule } from './material.module';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      MaterialModule
    ],
    exports: [
      CommonModule,
      MaterialModule
    ],
    declarations: [
    ]
  })
  export class SharedModule{}