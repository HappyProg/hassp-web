import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Product } from '../models/product.model';
import { ProductService } from '../Core/Services/product.service';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  isProgress = true;
  columnsToDisplay = ['prefix', 'code', 'article', 'name', 'type'];
  dataSource : MatTableDataSource<Product> = new MatTableDataSource<Product>();
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private productService : ProductService) { }

  ngOnInit(): void {
    this.productService.get().subscribe(
      data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;  
      }
    ).add(() => this.isProgress = false);
  }

}
