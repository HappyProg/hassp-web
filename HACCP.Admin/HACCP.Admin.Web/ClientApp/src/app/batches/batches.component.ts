import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Batch } from '../models/batch.model';
import { Product } from '../models/product.model';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-batches',
  templateUrl: './batches.component.html',
  styleUrls: ['./batches.component.scss']
})
export class BatchesComponent implements OnInit {
  isProgress = false;
  columnsToDisplay = ['number', 'amount', 'unitName', 'product'];
  dataSource : MatTableDataSource<Batch> = new MatTableDataSource<Batch>();
  batches : Batch[]  = [
    {
      id : "1",
      number : "GOV-107/05.03",
      amount : 7,
      unitName : "кг",
      product : "Говядина котлетная"
    }, 
    {
      id : "1",
      number : "SVN-101/05.03",
      amount : 11.300,
      unitName : "кг",
      product : "Свинина охлажденная"
    }, 
    {
      id : "1",
      number : "LUK-82/04.03",
      amount : 4.100,
      unitName : "кг",
      product : "Лук репчатый"
    }, 
    {
      id : "1",
      number : "FRG-107/05.03",
      amount : 5.350,
      unitName : "кг",
      product : "Фарш говяжий"
    }, 
    {
      id : "1",
      number : "KTL-107/05.03",
      amount : 1.700,
      unitName : "кг",
      product : "Котлеты по Киевски"
    }  
  ];

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor() { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.batches);
    this.dataSource.sort = this.sort; 
  }

}
