﻿using System.Threading.Tasks;
using HACCP.Admin.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using Microsoft.Extensions.Logging;

namespace HASSP.Admin.Web.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SessionsController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILogger _logger;

        public SessionsController(IUserService userService, ILogger<SessionsController> logger)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public async Task<IActionResult> Authenticate(ApplicationUserDTO model)
        {
            try
            {
                if (model == null)
                {
                    return BadRequest();
                }

                var user = await _userService.AuthenticateAsync(username: model.Username, password: model.Password);

                return Ok(user);
            }
            catch(UnauthorizedAccessException)
            {
                return Unauthorized("Ошибка авторизации! Обратитесь к администратору.");
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(500, "Внутренняя ошибка сервера");
            }
            
        }
    }
}