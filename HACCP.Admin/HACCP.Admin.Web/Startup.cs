using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Collections.Generic;
using HACCP.Admin.DAL;
using HACCP.Admin.Infrastructure;
using HACCP.Admin.BLL;
using HACCP.Admin.Interfaces;

namespace HACCP.Admin.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

#if DEBUG
            services.AddDbContext<ApplicationDbContext>(options => {
                options.UseSqlServer(Configuration.GetConnectionString("DebugConnection"));
            });
#else
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("ProdConnection")));
#endif

            services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddIdentityServer()
                .AddApiAuthorization<ApplicationUser, ApplicationDbContext>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = JWTManager.SecretKey,
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero
                    };
                })
                .AddIdentityServerJwt();

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .Build();
            });

            services.AddControllersWithViews();
            services.AddRazorPages();
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            // AutoMapper
            //
            services.AddAutoMapper(typeof(MapperProfileDTO), typeof(MapperProfileIIKO));

            // DI
            //
            services.AddScoped<IInvoiceService, InvoiceService>();
            services.AddScoped<ICookingService, CookingService>();
            services.AddScoped<ISettingsService, SettingsService>();
            services.AddScoped<IExternalImportService, IIKOService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRecipeCardService, RecipeCardService>();        
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseIdentityServer();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });

            DbInit<ApplicationDbContext>(app);

            //MokSettings(app);
            //TestLoadIIKO(app);
            //Test(app);
        }

        private void MokSettings(IApplicationBuilder app)
        {
            using (var serviceFactory = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var settingsService = serviceFactory.ServiceProvider.GetService<ISettingsService>();
                var settings = new ApplicationSettingsDTO
                {
                    Id = Guid.Parse("871F4D1B-D337-40C6-9F7E-655BD7ECF16F"),
                    IikoPassword = "resto#test8359",
                    IikoURL = "http://oookulinarserv.hldns.ru:9080",//"http://192.168.1.28:8085",
                    IikoUsername = "denvic"
                };

                settingsService.SaveAsync(settings).Wait();
            }
        }

        private void TestLoadIIKO(IApplicationBuilder app)
        {
            using (var serviceFactory = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var dateFrom = DateTime.Parse("2020-02-27");
                var dateto = DateTime.Parse("2020-02-27");

                var settingsService = serviceFactory.ServiceProvider.GetService<ISettingsService>();
                var productService = serviceFactory.ServiceProvider.GetService<IProductService>();
                var cookingService = serviceFactory.ServiceProvider.GetService<ICookingService>();
                var recipeCardService = serviceFactory.ServiceProvider.GetService<IRecipeCardService>();
                var iikoService = serviceFactory.ServiceProvider.GetService<IExternalImportService>();

                var cc = iikoService.GetProductCategoriesAsync().Result;
                var pp = iikoService.GetProductsAsync().Result;
                cc = productService.SaveAsync(cc).Result;
                pp = productService.SaveAsync(pp).Result;
                var settings = settingsService.GetAsync().Result;

                //var rcc = iikoService.GetRecipeCardsAsync(dateFrom, dateto, (int)(settings.IikoRecipeCardRevision ?? 0)).Result;
                //rcc = recipeCardService.CreateAsync(rcc).Result;

                //settings.IikoRecipeCardRevision = rcc.First().Revision;
                //settingsService.SaveAsync(settings).Wait();

                //var recipes = iikoService.GetCookingActs(dateFrom, dateto).Result;
                //foreach(var ca in recipes)
                //{
                //    cookingService.Create(ca.RecipeCard, ca.ResultAmount, ca.Store).Wait();
                //}


                //var rr = recipeCardService.CreateAsync(recipes).Result;
                //iikoService.GetCookingActs(DateTime.Parse("2020-02-26"), DateTime.Parse("2020-02-27")).Wait();
                //var invService = serviceFactory.ServiceProvider.GetService<IInvoiceService>();
                //var inv = invService.LoadFromExternalServiceAsync(DateTime.Parse("2020-01-01"), DateTime.Parse("2020-02-01")).Result;

                //var prodService = serviceFactory.ServiceProvider.GetService<IProductService>();

                //var inv = iikoService.LoadInvoicesAsync(DateTime.Parse("2020-01-01"), DateTime.Parse("2020-02-01")).Result;
                //var cc = iikoService.LoadCategoriesAsync().Result;
                //var pp = iikoService.LoadProductsAsync().Result;

                //var t = prodService.SaveAsync(cc).Result;
                //var tt = prodService.SaveAsync(pp).Result;
            }
        }

        //private void Test(IApplicationBuilder app)
        //{
        //    using (var serviceFactory = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
        //    {
        //        var ctx = serviceFactory.ServiceProvider.GetService<ApplicationDbContext>();
        //        var invoiceService = serviceFactory.ServiceProvider.GetService<IInvoiceService>();
        //        var cookingService = serviceFactory.ServiceProvider.GetService<ICookingService>();

        //        var test = "��������".Transliterate(3);
        //        var test2 = "��������".Transliterate();

        //        var store = new Store
        //        {
        //            Name = "�����1"
        //        };

        //        ctx.Stores.Add(store);

        //        var products = new List<Product>
        //        {
        //            new Product
        //            {
        //                Id = Guid.NewGuid(),
        //                Name = "��������",
        //                Prefix = "GOV"
        //            },
        //            new Product
        //            {
        //                Id = Guid.NewGuid(),
        //                Name = "�������",
        //                Prefix = "SVN"
        //            },
        //            new Product
        //            {
        //                Id = Guid.NewGuid(),
        //                Name = "���",
        //                Prefix = "LUK"
        //            },
        //            new Product
        //            {
        //                Id = Guid.NewGuid(),
        //                Name = "����",
        //                Prefix = "FAR"
        //            },
        //            new Product
        //            {
        //                Id = Guid.NewGuid(),
        //                Name = "�������",
        //                Prefix = "KTL"
        //            },
        //        };

        //        ctx.Products.AddRange(products);

        //        var um = new UnitMeasurement
        //        {
        //            Id = Guid.NewGuid(),
        //            Name = "��"
        //        };

        //        ctx.UnitMeasurements.Add(um);

        //        var receipt = new RecipeCard
        //        {
        //            Id = Guid.NewGuid(),
        //            Items = new List<RecipeItem>()
        //            {
        //                new RecipeItem
        //                {
        //                    Id = Guid.NewGuid(),
        //                    AmountIn = 1,
        //                    Product = products[0]                            
        //                },
        //                new RecipeItem
        //                {
        //                    Id = Guid.NewGuid(),
        //                    AmountIn = 1,
        //                    Product = products[2]
        //                }
        //            },
        //            ResultProduct = products[3]
        //        };

        //        ctx.RecipeCards.Add(receipt);
        //        ctx.SaveChanges();


        //        CreateInvoice(ctx, products, store, um);
        //        var batches = CreateInvoice(ctx, products, store, um);
        //        var recipe = new RecipeCardDTO
        //        {
        //            Id = receipt.Id,
        //            Items = new List<IRecipeItem>()
        //            {
        //                new RecipeItemDTO
        //                {
        //                    Id = Guid.NewGuid(),
        //                    AmountIn = 1,
        //                    Product = new ProductDTO
        //                    { 
        //                        Id = products[0].Id,
        //                        Name = products[0].Name
        //                    },
        //                },
        //                new RecipeItemDTO
        //                {
        //                    Id = Guid.NewGuid(),
        //                    AmountIn = 1,
        //                    Product = new ProductDTO
        //                    {
        //                        Id = products[2].Id,
        //                        Name = products[2].Name
        //                    },
        //                }
        //            },
        //            ResultProduct = new ProductDTO
        //            {
        //                Id = products[3].Id,
        //                Name = products[3].Name
        //            }
        //        };

        //        var cookingAct= cookingService.Create(recipe, 501, new StoreDTO { Id = store.Id, Name = store.Name }).Result;

        //        //var cookingBatch = CreateCooking(ctx, 5, 20, batches);
        //        //var cookingBatch2 = CreateCooking(ctx, 1, 3, new Batch[] { cookingBatch, batches.First() });


        //        //var t = ctx.AccumRegBatches
        //        //    .Include(i => i.Parent)
        //        //    .Include(i => i.Batch)
        //        //    .Include(i => i.Document)
        //        //    .Where(x => x.Batch.Id == cookingBatch2.Id)
        //        //    .ToList();

        //        var batchId = cookingAct.ResultBatch.Id;// Guid.Parse("2e728b1c-26b9-448c-aeaf-b00489f8c0cf");

        //        var rec = ctx.AccumRegBatches.First(x => x.BatchId == batchId);
        //        //var rf = ctx.AccumBatchRelations.First(x => x.ToId == rec.Id);
        //        //ctx.Entry(rf).Reference(s => s.AccumBatchFrom).LoadAsync().Wait();
        //        //ctx.Entry(rec).Collection(s => s.RelatedFrom).LoadAsync().Wait();

        //        var t = ctx.AccumBatchRelations
        //            .Where(x => x.ToId == rec.Id)
        //            .Include(i => i.AccumBatchFrom)
        //                .ThenInclude(i => i.RelatedFrom);

        //        //foreach(var tt in t)
        //        //{

        //        //    if (tt.AccumBatchFrom != null)
        //        //    {
        //        //        var y = ctx.AccumBatchRelations
        //        //            .Where(x => x.ToId == tt.ToId)
        //        //            .Include(i => i.AccumBatchFrom)
        //        //                .ThenInclude(i => i.RelatedFrom)
        //        //             .ToList();

        //        //        tt.AccumBatchFrom.RelatedFrom = y;
        //        //    }
                    
        //        //        //.FirstOrDefault(x => x.AccumBatchTo.Id == tt.ToId)?.AccumBatchFrom;
        //        //    foreach (var ttt in tt.AccumBatchFrom.RelatedFrom)
        //        //    {
        //        //        if (tt.AccumBatchFrom != null)
        //        //        {
        //        //            var y = ctx.AccumBatchRelations
        //        //                .Where(x => x.ToId == ttt.ToId)
        //        //                .Include(i => i.AccumBatchFrom)
        //        //                    .ThenInclude(i => i.RelatedFrom)
        //        //                 .ToList();

        //        //            ttt.AccumBatchFrom.RelatedFrom = y;
        //        //        }
        //        //    }
        //        //}

        //    }
        //}

        //private ICollection<AccumBatchRelation> GetRecursive<TSource>(IQueryable<AccumBatchRelation> source, long parentId, ApplicationDbContext ctx)
        ////private ICollection<AccumBatchRelation> GetRecursive<TSource>(IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        //{
        //    var result = source.Where(x => x.AccumBatchFrom.Id == parentId);
        //    if (result != null) 
        //    {
        //        foreach (var r in result)
        //        {
        //            r.AccumBatchFrom.RelatedFrom = ctx.AccumBatchRelations.Where(x => x.AccumBatchFrom.Id == r.ToId).ToList();

        //            GetRecursive(r, r.ToId, ctx);
        //        }

        //    }
        //}

        //private IEnumerable<Batch> CreateInvoice(ApplicationDbContext ctx, IEnumerable<Product> products, Store store, UnitMeasurement um)
        //{

        //    var inv = new Invoice()
        //    {
        //        Id = Guid.NewGuid(),
        //        Number = "333"
        //    };

        //    var r = new Random();
        //    var result = new List<Batch>();

        //    foreach (var p in products)
        //    {
        //        var q = r.Next(1, 999);
        //        var b = BatchBuilder.Create(ctx)
        //            .SetAmount(500)
        //            .SetDate(DateTime.Now)
        //            .SetProduct(p)
        //            .SetStore(store)
        //            .SetUnitMeasurement(um)
        //            .Build().Result;
                
        //        var bReg = new AccumRegBatch
        //        {
        //            Amount = b.Amount,
        //            Date = DateTime.Now,
        //            Batch = b,
        //            Document = inv,
        //            OperationType = OperationType.Inc
        //        };

        //        ctx.Batches.Add(b);
        //        ctx.AccumRegBatches.Add(bReg);

        //        result.Add(b);
        //    }

        //    ctx.SaveChanges();

        //    return result;
        //}

        //private Batch CreateCooking(ApplicationDbContext ctx, decimal AmountIn, decimal AmountOut, IEnumerable<Batch> batches)
        //{
        //    //// �������� ������ �������� ������� ����� ���������
        //    ////
        //    //var bReg = ctx.AccumRegBatches
        //    //    .Where(x => batches.Select(s => s.Id).Contains(x.BatchId))
        //    //    .ToList();

        //    var bSubCollection = new List<AccumRegBatch>();

        //    var cookAct = new CookingAct
        //    {
        //        Id = Guid.NewGuid(),
        //        Number = Guid.NewGuid().ToString().Substring(0, 3)
        //    };




        //    foreach (var b in batches)
        //    {
        //        // ��������
        //        //
        //        var bRegSub = new AccumRegBatch
        //        {
        //            Amount = AmountIn * -1,
        //            Date = DateTime.Now,
        //            Batch = b,
        //            //Children = new List<AccumRegBatch>(),
        //            Document = cookAct,
        //            OperationType = OperationType.Cooking
        //        };

        //        // ��������� ������ � �������
        //        //
        //        var bReg = ctx.AccumRegBatches
        //            .OrderBy(o => o.Id)
        //            .First(x => x.BatchId == b.Id && x.Amount > 0);

        //        //bRegSub.Children.Add(bReg);

        //        bRegSub.Batch.CurrentAmount += AmountIn * -1;
        //        ctx.AccumRegBatches.Add(bRegSub);

        //        bSubCollection.Add(bRegSub);

        //        ctx.AccumBatchRelations.Add(new AccumBatchRelation
        //        {
        //            AccumBatchFrom = bReg,
        //            AccumBatchTo = bRegSub
        //        });
        //    }


        //    // ����� ������
        //    //
        //    var newBatch = new Batch
        //    {
        //        Id = Guid.NewGuid(),
        //        Amount = AmountOut,
        //        CurrentAmount = AmountOut,
        //        Date = DateTime.Now,
        //        BatchIndex = 7
        //    };

        //    // ����� ������ � ��������
        //    //
        //    var bRegNew = new AccumRegBatch
        //    {
        //        Amount = AmountOut,
        //        Date = DateTime.Now,
        //        Batch = newBatch,
        //        //Children = bSubCollection,
        //        Document = cookAct,
        //        OperationType = OperationType.Cooking
        //    };


        //    foreach (var b in bSubCollection)
        //    {
        //        ctx.AccumBatchRelations.Add(new AccumBatchRelation
        //        {
        //            AccumBatchFrom = b,
        //            AccumBatchTo = bRegNew
        //        });
        //    }
        //    ctx.Batches.Add(newBatch);
        //    ctx.AccumRegBatches.Add(bRegNew);

        //    ctx.SaveChanges();

        //    return newBatch;
        //}

        private void DbInit<T>(IApplicationBuilder app) where T : DbContext
        {
            using (var serviceFactory = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var ctx = serviceFactory.ServiceProvider.GetService<T>();
                ctx.Database.Migrate();
            }
        }
    }
}
