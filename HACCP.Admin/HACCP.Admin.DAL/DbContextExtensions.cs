﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HACCP.Admin.DAL
{
    public static class DbContextExtensions
    {
        //public static IIncludableQueryable<TEntity, TProperty> IncludeRecursive<TEntity, TProperty>([NotNull] this IQueryable<TEntity> source, [NotNull] Expression<Func<TEntity, TProperty>> navigationPropertyPath) where TEntity : class
        //{
        //    source.Provider.CreateQuery( (navigationPropertyPath);
        //    foreach (var t in source)
        //    {

        //    }
        //}


        //        public T LoadRecursive<T>(this IQueryable<T> item, Expression<Func<T, IQueryable>> loadSelector)
        //{
        //            item
        //            .Collection(loadSelector)
        //            .Load();

        //    foreach (var child in loadSelector.Compile().Invoke(item))
        //        LoadRecursive(child, loadSelector);
        //    return item;
        //}

        //public static IEnumerable<TSource> SelectRecursive<TSource>(this IEnumerable<TSource> source, Func<TSource, IEnumerable<TSource>> selector)
        //    {
        //        var result = source.SelectMany(selector);
        //        if (!result.Any())
        //        {
        //            return result;
        //        }
        //        return result.SelectRecursive(selector);
        //    }

        //public static IQueryable<TEntity> WhereContains<TEntity, TEntityDTO, TValue>(this DbSet<TEntity> dbSet, IEnumerable<TEntityDTO> dto, Func<TEntityDTO, object> selector, Func<TEntity, TValue> propertyAccess)
        //    where TEntity : class
        //{
        //    var Ids = dto.Select(selector).ToArray();

        //    //var entities = await dbSet
        //    //    .Where(x => Ids.Contains(propertyAccess))
        //    //    .ToArrayAsync();

        //    return dbSet.Where(x => Ids.Contains(propertyAccess));
        //    //return obj.Where(o => container.Contains(propertyAccess(o)));
        //}


        public static IEnumerable<T> WhereContains<T, TValue>(this IQueryable<T> dbSet, IEnumerable<TValue> container, Func<T, TValue> propertyAccess)
        {
            return dbSet
                .AsEnumerable()
                .Where(x => container.Contains(propertyAccess(x)));
        }
    }
}
