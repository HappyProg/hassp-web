﻿using HACCP.Admin.Interfaces;
using System;
using System.Threading.Tasks;

namespace HACCP.Admin.DAL
{
    public class BatchBuilder
    {
        private DateTime _currentDate;
        private decimal _amount;
        private DateTime? _date;
        private Product _product;
        private Store _store;
        private UnitMeasurement _unitMeasurement;
        private string _suplierCode;
        private readonly ApplicationDbContext _database;
        private string _number;
        private string _comment;
        private string _typeCreaterBatch;

        public BatchBuilder(ApplicationDbContext database)
        {
            _database = database;
            _currentDate = DateTime.Now;
        }

        public static BatchBuilder Create(ApplicationDbContext database)
        {
            return new BatchBuilder(database);
        }

        public BatchBuilder SetProduct(Product product)
        {
            _product = product;
            return this;
        }

        public BatchBuilder SetAmount(decimal amount)
        {
            _amount = amount;
            return this;
        }

        public BatchBuilder SetDate(DateTime date)
        {
            _date = date;
            return this;
        }

        public BatchBuilder SetStore(Store store)
        {
            _store = store;
            return this;
        }

        public BatchBuilder SetUnitMeasurement(UnitMeasurement val)
        {
            _unitMeasurement = val;
            return this;
        }

        public BatchBuilder SetSuplierCode(string val)
        {
            _suplierCode = val;
            return this;
        }

        /// <summary>
        /// Тип партии, необязательный параметр, если задан то попадет в номер партии
        /// например INC - incoming и т.д.
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public BatchBuilder SetTypeCreater(OperationType val)
        {
            _typeCreaterBatch = GetTypeDocument(val);
            return this;
        }

        public BatchBuilder SetNumber(string val)
        {
            _number = val;
            return this;
        }

        public BatchBuilder SetComment(string val)
        {
            _comment = val;
            return this;
        }

        public async Task<Batch> Build()
        {
            if (_product == null)
            {
                throw new InvalidOperationException("Не указан продукт партии!");
            }
            if (_store == null)
            {
                throw new InvalidOperationException("Не указан склад размещения партии!");
            }
            if (_unitMeasurement == null)
            {
                throw new InvalidOperationException("Не указана ед. измерения партии!");
            }
           
            var batchIndex = await GetBatchIndex();
            if (string.IsNullOrWhiteSpace(_number))
            {
                _number = GenerateNumber(batchIndex);
            }
           
            return new Batch
            {
                Id = Guid.NewGuid(),
                Amount = _amount,
                CurrentAmount = _amount,
                Date = _date ?? _currentDate,
                DateCreated = _currentDate,
                DateModified = _currentDate,
                Product = _product,
                Store = _store,
                UnitMeasurement = _unitMeasurement,
                BatchIndex = batchIndex,
                Number = _number,
                Comment = _comment
            };
        }

        private Task<int> GetBatchIndex()
        {
            return  _database.GetSequence(Const.BATCH_SEQUENCE);
        }

        private string GenerateNumber(int batchIndex)
        {           
            var prefix = _product.Prefix;

            if (string.IsNullOrWhiteSpace(prefix))
            {
                throw new InvalidOperationException($"Для товара \"{_product.Name}\", code - {_product.Code}, артикул - {_product.Article} не задан префикс!");
            }

            var type = string.IsNullOrWhiteSpace(_typeCreaterBatch) ? "" : _typeCreaterBatch + "-";
            return $"{type}{prefix}-{batchIndex}/{(_date ?? _currentDate).ToString("dd.MM")}";
        }

        private string GetTypeDocument(OperationType val)
        {
            switch (val)
            {
                case OperationType.Inc:
                    return "INC";
                case OperationType.Cooking:
                    return "COOK";
                case OperationType.Out:
                    return "OUT";
                case OperationType.Sale:
                    return "SAL";
                case OperationType.Substract:
                    return "SUB";
                default:
                    throw new InvalidOperationException($"Неизвестный тип документа {val}!");
            }
        }
    }
}
