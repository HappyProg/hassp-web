﻿namespace HACCP.Admin.DAL
{
    internal static partial class Const
    {
        public const string BATCH_SEQUENCE = "BatchSequence";
        public const string CA_SEQUENCE = "CASequence";
    }
}
