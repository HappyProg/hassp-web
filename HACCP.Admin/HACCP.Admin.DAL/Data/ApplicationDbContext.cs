﻿using HACCP.Admin.Interfaces;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace HACCP.Admin.DAL
{
    public class ApplicationDbContext : KeyApiAuthorizationDbContext<ApplicationUser, ApplicationRole, Guid>
    {

        public virtual DbSet<ApplicationSettings> Settings { get; set; }
        public virtual DbSet<RecipeCard> RecipeCards { get; set; }
        public virtual DbSet<AccumBatchRelation> AccumBatchRelations { get; set; }
        public virtual DbSet<Store> Stores { get; set; }
        public virtual DbSet<UnitMeasurement> UnitMeasurements { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<CookingAct> CookingActs { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<Batch> Batches { get; set; }
        public virtual DbSet<AccumRegBatch> AccumRegBatches { get; set; }

        public ApplicationDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }

        public async Task<int> GetSequence(string sequenceName)
        {
            var result = new SqlParameter("@result", System.Data.SqlDbType.Int)
            {
                Direction = System.Data.ParameterDirection.Output
            };

            await Database.ExecuteSqlRawAsync(
                       $"SELECT @result = (NEXT VALUE FOR {sequenceName})", result);

            return (int)result.Value;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Product>(e =>
            {
                e.HasOne(r => r.Parent).WithMany(u => u.Children).HasForeignKey(r => r.ParentId).OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<AccumBatchRelation>(e =>
            {
                //e.HasKey(e => new { e.FromId, e.ToId });
                e.HasOne(r => r.AccumBatchFrom).WithMany(u => u.RelatedTo).IsRequired().HasForeignKey(r => r.FromId).OnDelete(DeleteBehavior.Restrict);

                e.HasOne(r => r.AccumBatchTo).WithMany(u => u.RelatedFrom).IsRequired().HasForeignKey(r => r.ToId).OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<Batch>()
                .Property(o => o.BatchIndex)
                .HasDefaultValueSql($"NEXT VALUE FOR {Const.BATCH_SEQUENCE}");

            builder.HasSequence<int>(Const.BATCH_SEQUENCE)
                       .StartsAt(1)
                       .IncrementsBy(1);

            builder.HasSequence<int>(Const.CA_SEQUENCE)
                       .StartsAt(1)
                       .IncrementsBy(1);

            UserInit(builder);

            base.OnModelCreating(builder);
        }


        #region Data init
        private static void UserInit(ModelBuilder builder)
        {
            var role = new ApplicationRole
            {
                Id = Guid.Parse("8C38FC8B-24FB-4644-8494-D15FFFD21B27"),
                Name = "Администратор",
                NormalizedName = "АДМИНИСТРАТОР",
                RoleType = RoleType.Admin,
                ConcurrencyStamp = "8C38FC8B-24FB-4644-8494-D15FFFD21B27"
            };
            builder.Entity<ApplicationRole>().HasData(role);

            var passwordHasher = new PasswordHasher<ApplicationUser>();
            builder.Entity<ApplicationUser>().HasData(new
            {
                Id = Guid.Parse("8C38FC8B-24FB-4644-8494-D15FFFD21BFB"),
                Email = "dr.happy.prog@gmail.com",
                EmailConfirmed = true,
                FirstName = "Happy",
                LastName = "Prog",
                NormalizedEmail = "DR.HAPPY.PROG@GMAIL.COM",
                PasswordHash = passwordHasher.HashPassword(null, "123"),
                UserName = "dr.happy.prog@gmail.com",
                NormalizedUserName = "DR.HAPPY.PROG@GMAIL.COM",
                SecurityStamp = "8C38FC8B-24FB-4644-8494-D15FFFD21BFB",
                PhoneNumber = "",
                AccessFailedCount = 0,
                ConcurrencyStamp = "",
                LockoutEnabled = false,
                LockoutEnd = DateTimeOffset.Parse("10.10.2018"),
                PasswordSalt = "",
                Patron = "",
                PhoneNumberConfirmed = true,
                TwoFactorEnabled = false,
                RoleId = role.Id
            });
        }

        #endregion
    }
}
