﻿using HACCP.Admin.Interfaces;
using System.Diagnostics.CodeAnalysis;

namespace HACCP.Admin.DAL
{
    public class UnitMeasurement : BaseRefEntity, IEqutableDTO<IUnitMeasurement>
    {
        public bool Equals([AllowNull] IUnitMeasurement other)
        {
            return other.Name?.ToUpper() == Name?.ToUpper()
                && other.IsDeleted == IsDeleted;
        }
    }
}
