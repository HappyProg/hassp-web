﻿using HACCP.Admin.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace HACCP.Admin.DAL
{
    public class Product : BaseRefEntity, IEqutableDTO<IProduct>
    {
        public string Prefix { get; set; }
        /// <summary>
        /// Артикул
        /// </summary>
        public string Article { get; set; }
        /// <summary>
        /// Код товара
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Кол-во остатка для незамедлительного списания
        /// </summary>
        public decimal MinimumBalanceToClear { get; set; }
        public ProductType ProductType { get; set; }
        public Guid? ParentId { get; set; }
        public Product Parent { get; set; }
        public ICollection<Product> Children { get; set; }

        public bool Equals([AllowNull] IProduct other)
        {
            return other.Name == Name
                && other.ParentId == ParentId
                && other.ProductType == ProductType
                && other.IsDeleted == IsDeleted
                && other.Article == Article
                && other.Code == Code
                && other.Comment == Comment;
        }
    }
}
