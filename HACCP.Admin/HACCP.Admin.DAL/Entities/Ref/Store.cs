﻿using HACCP.Admin.Interfaces;
using System.Diagnostics.CodeAnalysis;

namespace HACCP.Admin.DAL
{
    public class Store : BaseRefEntity, IEqutableDTO<IStore>
    {
        public bool Equals([AllowNull] IStore other)
        {
            return other.Name?.ToUpper() == Name?.ToUpper()
                && other.IsDeleted == IsDeleted;
        }
    }
}
