﻿using System;
using System.Collections.Generic;

namespace HACCP.Admin.DAL
{
    public class RecipeCard : BaseEntity
    {
        public Product ResultProduct { get; set; }
        public Guid ResultProductId { get; set; }
        public decimal Coefficient { get; set; } = 1;
        public ICollection<RecipeItem> Items { get; set; }
    }
}
