﻿using System;

namespace HACCP.Admin.DAL
{
    public class CookingAct: Document
    {
        public int CookingActIndex { get; set; }
        public Batch ResultBatch { get; set; }
        /// <summary>
        /// Тех. карта (рецепт)
        /// </summary>
        public RecipeCard RecipeCard { get; set; }
        /// <summary>
        /// Выход блюда
        /// </summary>
        public decimal ResultAmount { get; set; }
        public UnitMeasurement UnitMeasurement { get; set; }
        public Guid UnitMeasurementId { get; set; }
    }
}
