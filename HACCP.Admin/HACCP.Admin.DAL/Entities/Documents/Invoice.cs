﻿using HACCP.Admin.Interfaces;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace HACCP.Admin.DAL
{
    public class Invoice : Document, IEqutableDTO<IInvoice>
    {
        ///// <summary>
        ///// Партии
        ///// </summary>
        //public ICollection<AccumRegBatch> Batches { get; set; }
        /// <summary>
        /// Товары
        /// </summary>
        public ICollection<DocumentProductItem> Items { get; set; }

        public bool Equals([AllowNull] IInvoice other)
        {
            return this.Number == other.Number;
        }
    }
}
