﻿using System;

namespace HACCP.Admin.DAL
{
    /// <summary>
    /// Товарная позиция документа
    /// </summary>
    public class DocumentProductItem : BaseEntity
    {
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public decimal VatPercent { get; set; }
        public decimal VatAmount { get; set; }
        public Guid UnitMeasurementId { get; set; }
        public UnitMeasurement UnitMeasurement { get; set; }
        /// <summary>
        /// Склад хранения
        /// </summary>
        public Guid StoreId { get; set; }
        public Store Store { get; set; }
    }
}
