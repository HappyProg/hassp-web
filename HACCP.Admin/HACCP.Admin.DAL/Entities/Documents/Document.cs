﻿using System;

namespace HACCP.Admin.DAL
{
    public class Document : BaseEntity
    {
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public ApplicationUser Owner { get; set; }
        /// <summary>
        /// Признак синхронизации с внешней системой
        /// </summary>
        public bool IsSync { get; set; }
        public Store Store { get; set; }
        public Guid StoreId { get; set; }
    }
}
