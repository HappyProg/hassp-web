﻿using HACCP.Admin.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;

namespace HACCP.Admin.DAL
{
    public class ApplicationRole : IdentityRole<Guid>
    {
        public RoleType RoleType { get; set; }
    }
}
