﻿using System;

namespace HACCP.Admin.DAL
{
    public class RecipeItem : BaseEntity
    {
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
        /// <summary>
        /// Брутто (в основных единицах измерения ингредиента). Именно это поле участвует в расчете списаний
        /// </summary>
        public decimal AmountIn { get; set; }
        /// <summary>
        /// Нетто (в основных единицах измерения ингредиента)
        /// </summary>
        public decimal AmountMiddle { get; set; }
        /// <summary>
        /// Выход готового продукта (в основных единицах измерения ингредиента)
        /// </summary>
        public decimal AmountOut { get; set; }
    }
}
