﻿using System;
using System.Collections.Generic;

namespace HACCP.Admin.DAL
{
    public class AccumRegBatch : BaseAccumReg
    {
        /// <summary>
        /// Документ движения партии (партию может двигать только документ, например приходная накладная, акт приготовления, реализация и т.д.)
        /// </summary>
        public Document Document { get; set; }
        /// <summary>
        /// Внешний ключ (Документ)
        /// </summary>
        public Guid DocumentId { get; set; }
        /// <summary>
        /// Внешний ключ
        /// </summary>
        public Guid BatchId { get; set; }
        /// <summary>
        /// Партия
        /// </summary>
        public Batch Batch { get; set; }
        /// <summary>
        /// Кол-во +/-
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// Зависимости записи (приход, списание)
        /// </summary>                
        //public ICollection<AccumRegBatch> Children { get; set; } = new List<AccumRegBatch>();

        //[InverseProperty(nameof(AccumBatchRelation.AccumBatchTo))]
        //[InverseProperty("AccumBatchTo")]
        public ICollection<AccumBatchRelation> RelatedTo { get; set; }

        //[InverseProperty("AccumBatchFrom")]
        public ICollection<AccumBatchRelation> RelatedFrom { get; set; }
    }
}
