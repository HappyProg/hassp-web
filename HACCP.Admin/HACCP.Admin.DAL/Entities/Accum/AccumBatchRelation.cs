﻿using System.ComponentModel.DataAnnotations.Schema;

namespace HACCP.Admin.DAL
{
    public class AccumBatchRelation
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long FromId { get; set; }
        public long ToId { get; set; }
        [ForeignKey("ToId")]
        public AccumRegBatch AccumBatchFrom { get; set; }
        [ForeignKey("FromId")]
        public AccumRegBatch AccumBatchTo { get; set; }
    }
}
