﻿namespace HACCP.Admin.DAL
{
    public class ApplicationSettings : BaseEntity
    {
        public string IikoURL { get; set; }
        public string IikoUsername { get; set; }
        public string IikoPassword { get; set; }
        public long? IikoRecipeCardRevision { get; set; }
    }
}
