﻿using System;

namespace HACCP.Admin.DAL
{
    /// <summary>
    /// Партии товаров
    /// </summary>
    public class Batch : BaseEntity
    {
        /// <summary>
        /// Дата партии (может отличаться от даты создания)
        /// </summary>
        public DateTime Date { get; set; }
        public string Number { get; set; }
        public Product Product { get; set; }
        /// <summary>
        /// Номер партии - сквозной, автоинкремент
        /// </summary>
        public int BatchIndex { get; set; }
        /// <summary>
        /// Количество
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// Текущее кол-во, отражает кол-во партии с учетом всех списаний, готовок, корректировок и т.д.
        /// </summary>
        public decimal CurrentAmount { get; set; }
        /// <summary>
        /// Единица измерения
        /// </summary>
        public UnitMeasurement UnitMeasurement { get; set; }
        /// <summary>
        /// Склад хранения
        /// </summary>
        public Store Store { get; set; }
    }
}
