﻿namespace HACCP.Admin.DAL
{
    public abstract class BaseRefEntity : BaseEntity
    {
        public string Name { get; set; }
    }
}
