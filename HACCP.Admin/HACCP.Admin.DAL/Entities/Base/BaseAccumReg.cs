﻿using HACCP.Admin.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HACCP.Admin.DAL
{
    public abstract class BaseAccumReg
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
        public OperationType OperationType { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
