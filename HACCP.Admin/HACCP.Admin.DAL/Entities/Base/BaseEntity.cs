﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HACCP.Admin.DAL
{
    public abstract class BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }        
        public bool IsDeleted { get; set; }
        public string Comment { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
