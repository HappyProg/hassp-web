﻿using Microsoft.AspNetCore.Identity;
using System;

namespace HACCP.Admin.DAL
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patron { get; set; }
        public string PasswordSalt { get; set; }
        public ApplicationRole Role { get; set; }
    }
}
