﻿using Newtonsoft.Json;
using System;

namespace HACCP.IIKO.Infrastructure
{
    public class IikoProductDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("deleted")]
        public bool Deleted { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("num")]
        public string Num { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("parent")]
        public Guid? Parent { get; set; }

        [JsonProperty("modifiers")]
        public Modifier[] Modifiers { get; set; }

        [JsonProperty("taxCategory")]
        public Guid? TaxCategory { get; set; }

        [JsonProperty("category")]
        public Guid? Category { get; set; }

        [JsonProperty("accountingCategory")]
        public Guid? AccountingCategory { get; set; }

        [JsonProperty("color")]
        public RGBColorDto Color { get; set; }

        [JsonProperty("fontColor")]
        public RGBColorDto FontColor { get; set; }

        [JsonProperty("frontImageId")]
        public Guid? FrontImageId { get; set; }

        [JsonProperty("position")]
        public object Position { get; set; }

        [JsonProperty("mainUnit")]
        public Guid? MainUnit { get; set; }

        [JsonProperty("excludedSections")]
        public object ExcludedSections { get; set; }

        [JsonProperty("defaultSalePrice")]
        public decimal DefaultSalePrice { get; set; }

        [JsonProperty("placeType")]
        public Guid? PlaceType { get; set; }

        [JsonProperty("defaultIncludedInMenu")]
        public bool DefaultIncludedInMenu { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("unitWeight")]
        public decimal UnitWeight { get; set; }

        [JsonProperty("unitCapacity")]
        public decimal UnitCapacity { get; set; }

        [JsonProperty("notInStoreMovement")]
        public bool NotInStoreMovement { get; set; }
    }

    public class RGBColorDto
    {
        [JsonProperty("red")]
        public object Red { get; set; }

        [JsonProperty("green")]
        public object Green { get; set; }

        [JsonProperty("blue")]
        public object Blue { get; set; }
    }

    public class Modifier
    {
        [JsonProperty("modifier")]
        public Guid? ModifierModifier { get; set; }

        [JsonProperty("defaultAmount")]
        public int DefaultAmount { get; set; }

        [JsonProperty("freeOfChargeAmount")]
        public int FreeOfChargeAmount { get; set; }

        [JsonProperty("minimumAmount")]
        public int MinimumAmount { get; set; }

        [JsonProperty("maximumAmount")]
        public int MaximumAmount { get; set; }

        [JsonProperty("hideIfDefaultAmount")]
        public bool HideIfDefaultAmount { get; set; }

        [JsonProperty("required")]
        public bool ModifierRequired { get; set; }

        [JsonProperty("childModifiersHaveMinMaxRestrictions")]
        public bool ChildModifiersHaveMinMaxRestrictions { get; set; }

        [JsonProperty("splittable")]
        public bool Splittable { get; set; }

        [JsonProperty("childModifiers")]
        public Modifier[] ChildModifiers { get; set; }
    }
}
