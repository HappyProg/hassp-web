﻿using System;
using System.Xml.Serialization;

namespace HACCP.IIKO.Infrastructure
{
	[XmlRoot(ElementName = "item")]
	public class IIKOCookingActItemDTO
	{
		[XmlElement(ElementName = "amountUnit")]
		public Guid? AmountUnit { get; set; }
		[XmlElement(ElementName = "containerId")]
		public Guid? ContainerId { get; set; }
		[XmlElement(ElementName = "num")]
		public string Num { get; set; }
		[XmlElement(ElementName = "product")]
		public Guid ProductId { get; set; }
		[XmlElement(ElementName = "amount")]
		public decimal Amount { get; set; }

		[XmlIgnore]
		public string UnitMeasureName { get; set; }
		[XmlIgnore]
		public string StoreName { get; set; }
	}

	[XmlRoot(ElementName = "items")]
	public class IIKOCookingActItemsDTO
	{
		[XmlElement(ElementName = "item")]
		public IIKOCookingActItemDTO[] Products { get; set; }
	}

	[XmlRoot(ElementName = "document")]
	public class IIKOCookingActDTO
	{
		[XmlElement(ElementName = "storeFrom")]
		public Guid? StoreFrom { get; set; }
		[XmlElement(ElementName = "storeTo")]
		public Guid? StoreTo { get; set; }
		/// <summary>
		/// Дата документа
		/// </summary>
		[XmlElement(ElementName = "dateIncoming")]
		public DateTime DateIncoming { get; set; }
		[XmlElement(ElementName = "documentNumber")]
		public string DocumentNumber { get; set; }
		[XmlElement(ElementName = "comment")]
		public string Comment { get; set; }
		[XmlElement(ElementName = "items")]
		public IIKOCookingActItemsDTO Items { get; set; }
	}
}
