﻿using Newtonsoft.Json;
using System;

namespace HACCP.IIKO.Infrastructure
{
    public partial class IIKOReceiptCardDTO
    {
        [JsonProperty("knownRevision")]
        public int KnownRevision { get; set; }

        [JsonProperty("assemblyCharts")]
        public IIKOAssemblyChart[] AssemblyCharts { get; set; }

        [JsonProperty("preparedCharts")]
        public object PreparedCharts { get; set; }

        [JsonProperty("deletedAssemblyChartIds")]
        public object DeletedAssemblyChartIds { get; set; }

        [JsonProperty("deletedPreparedChartIds")]
        public object DeletedPreparedChartIds { get; set; }
    }

    public partial class IIKOAssemblyChart
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("assembledProductId")]
        public Guid AssembledProductId { get; set; }

        [JsonProperty("dateFrom")]
        public DateTimeOffset? DateFrom { get; set; }

        [JsonProperty("dateTo")]
        public DateTimeOffset? DateTo { get; set; }

        [JsonProperty("assembledAmount")]
        public decimal AssembledAmount { get; set; }

        [JsonProperty("effectiveDirectWriteoffStoreSpecification")]
        public IIKOAssemblyChartStoreSpecification EffectiveDirectWriteoffStoreSpecification { get; set; }

        [JsonProperty("productSizeAssemblyStrategy")]
        public string ProductSizeAssemblyStrategy { get; set; }

        [JsonProperty("items")]
        public IIKOAssemblyChartItem[] Items { get; set; }

        [JsonProperty("technologyDescription")]
        public string TechnologyDescription { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("appearance")]
        public string Appearance { get; set; }

        [JsonProperty("organoleptic")]
        public string Organoleptic { get; set; }

        [JsonProperty("outputComment")]
        public string OutputComment { get; set; }
    }

    public partial class IIKOAssemblyChartStoreSpecification
    {
        [JsonProperty("departments")]
        public object[] Departments { get; set; }

        [JsonProperty("inverse")]
        public bool Inverse { get; set; }
    }

    public partial class IIKOAssemblyChartItem
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("sortWeight")]
        public long SortWeight { get; set; }

        [JsonProperty("productId")]
        public Guid ProductId { get; set; }

        [JsonProperty("productSizeSpecification")]
        public Guid? ProductSizeSpecification { get; set; }

        [JsonProperty("storeSpecification")]
        public IIKOAssemblyChartStoreSpecification StoreSpecification { get; set; }

        [JsonProperty("amountIn")]
        public decimal AmountIn { get; set; }

        [JsonProperty("amountMiddle")]
        public decimal AmountMiddle { get; set; }

        [JsonProperty("amountOut")]
        public decimal AmountOut { get; set; }

        [JsonProperty("amountIn1")]
        public decimal? AmountIn1 { get; set; }

        [JsonProperty("amountOut1")]
        public decimal? AmountOut1 { get; set; }

        [JsonProperty("amountIn2")]
        public decimal? AmountIn2 { get; set; }

        [JsonProperty("amountOut2")]
        public decimal? AmountOut2 { get; set; }

        [JsonProperty("amountIn3")]
        public decimal? AmountIn3 { get; set; }

        [JsonProperty("amountOut3")]
        public decimal? AmountOut3 { get; set; }

        [JsonProperty("packageCount")]
        public decimal PackageCount { get; set; }

        [JsonProperty("packageTypeId")]
        public object PackageTypeId { get; set; }
    }
}
