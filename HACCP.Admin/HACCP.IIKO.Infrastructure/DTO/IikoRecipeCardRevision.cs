﻿namespace HACCP.IIKO.Infrastructure
{
    public class IikoRecipeCardRevision
    {
        public IIKOReceiptCardDTO[] RecipeCards { get; set; }
        public long Revision { get; set; }
    }
}
