﻿using System;
using System.Xml.Serialization;

namespace HACCP.IIKO.Infrastructure
{
	[XmlRoot(ElementName = "item")]
	public class IIKOInvoiceItemDTO
	{
		[XmlElement(ElementName = "actualAmount")]
		public decimal ActualAmount { get; set; }
		[XmlElement(ElementName = "store")]
		public Guid? Store { get; set; }
		[XmlElement(ElementName = "code")]
		public string Code { get; set; }
		[XmlElement(ElementName = "price")]
		public decimal Price { get; set; }
		[XmlElement(ElementName = "sum")]
		public decimal Sum { get; set; }
		[XmlElement(ElementName = "vatPercent")]
		public decimal VatPercent { get; set; }
		[XmlElement(ElementName = "vatSum")]
		public decimal VatSum { get; set; }
		[XmlElement(ElementName = "discountSum")]
		public decimal DiscountSum { get; set; }
		/// <summary>
		/// Базовая единица измерения
		/// </summary>
		[XmlElement(ElementName = "amountUnit")]
		public Guid? AmountUnit { get; set; }
		/// <summary>
		/// Номер позиции в документе. Обязательное поле
		/// </summary>
		[XmlElement(ElementName = "num")]
		public int Num { get; set; }
		/// <summary>
		/// Товар (guid). Хотя бы одно из полей должно быть заполнено: product или productArticle
		/// </summary>
		[XmlElement(ElementName = "product")]
		public Guid? Product { get; set; }
		/// <summary>
		/// Товар (артикул). Можно задать вместо guid товара начиная с 5.0, guid имеет приоритет
		/// </summary>
		[XmlElement(ElementName = "productArticle")]
		public string ProductArticle { get; set; }
		[XmlElement(ElementName = "amount")]
		public decimal Amount { get; set; }
		/// <summary>
		/// Фасовка (guid)
		/// </summary>
		[XmlElement(ElementName = "containerId")]
		public Guid? ContainerId { get; set; }
	}

	[XmlRoot(ElementName = "items")]
	public class InvoiceItemsDTO
	{
		[XmlElement(ElementName = "item")]
		public IIKOInvoiceItemDTO[] Products { get; set; } = new IIKOInvoiceItemDTO[] { };
	}

	[XmlRoot(ElementName = "document")]
	public class IIKOInvoiceDTO
	{
		[XmlElement(ElementName = "id")]
		public Guid Id { get; set; }
		[XmlElement(ElementName = "incomingDocumentNumber")]
		public string IncomingDocumentNumber { get; set; }
		[XmlElement(ElementName = "incomingDate")]
		public DateTime IncomingDate { get; set; }
		[XmlElement(ElementName = "useDefaultDocumentTime")]
		public bool? UseDefaultDocumentTime { get; set; }
		[XmlElement(ElementName = "dueDate")]
		public object DueDate { get; set; }
		[XmlElement(ElementName = "supplier")]
		public Guid? Supplier { get; set; }
		[XmlElement(ElementName = "defaultStore")]
		public Guid? DefaultStore { get; set; }
		/// <summary>
		/// Номер счет-фактуры
		/// </summary>
		[XmlElement(ElementName = "invoice")]
		public string Invoice { get; set; }
		[XmlElement(ElementName = "documentNumber")]
		public string DocumentNumber { get; set; }
		[XmlElement(ElementName = "comment")]
		public string Comment { get; set; }
		[XmlElement(ElementName = "status")]
		public string Status { get; set; }
		[XmlElement(ElementName = "items")]
		public InvoiceItemsDTO Items { get; set; }
	}

	[XmlRoot(ElementName = "incomingInvoiceDtoes")]
	public class IncomingInvoiceDtoes
	{
		[XmlElement(ElementName = "document")]
		public IIKOInvoiceDTO[] Document { get; set; } = new IIKOInvoiceDTO[] { };
	}


}
