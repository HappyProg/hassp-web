﻿using HACCP.Admin.Infrastructure;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HACCP.IIKO.Infrastructure
{

    public class IikoAPIService
    {
        private readonly string _host;
        private readonly string _password;
        private readonly string _username;

        private static class IIKOConst
        {
            public const string OLAP_COOKING_ACT_NAME = "Акт приготовления";
        }
        
        public IikoAPIService(string host, string username, string password)
        {
            if (string.IsNullOrWhiteSpace(host))
            {
                throw new ArgumentException("message", nameof(host));
            }

            if (string.IsNullOrWhiteSpace(username))
            {
                throw new ArgumentException("message", nameof(username));
            }

            _host = host;
            _password = password;
            _username = username;
        }

        public async Task<IEnumerable<IIKOInvoiceDTO>> GetInvoicesAsync(DateTime dateFrom, DateTime dateTo)
        {
            var query = $"/resto/api/documents/export/incomingInvoice?from={dateFrom.ToString("yyyy-MM-dd")}&to={dateTo.ToString("yyyy-MM-dd")}";
            var result = await LoadXml<IncomingInvoiceDtoes>(query);
            return result.Document;
        }

        public Task<IIKOReceiptCardDTO> GetRecipeCardsAsync(DateTime from, DateTime to, int revision = 0)
        {

                var query = revision <= 1 
                ? $"/resto/api/v2/assemblyCharts/getAll?dateFrom={from.ToString("yyyy-MM-dd")}&dateTo={to.ToString("yyyy-MM-dd")}"
                : $"/resto/api/v2/assemblyCharts/getAllUpdate?knownRevision={revision}&dateFrom={from.ToString("yyyy-MM-dd")}&dateTo={to.ToString("yyyy-MM-dd")}";

            return LoadJson<IIKOReceiptCardDTO>(query);
        }
        
        public Task<IikoProductDTO[]> GetProductCategoriesAsync()
        {
            var query = "/resto/api/v2/entities/products/group/list?includeDeleted=true";
            return LoadJson<IikoProductDTO[]>(query);
        }

        public Task<IikoProductDTO[]> GetProductsAsync()
        {
            var query = "/resto/api/v2/entities/products/list?types=DISH&types=GOODS&types=MODIFIER&types=PREPARED&includeDeleted=true";
            return LoadJson<IikoProductDTO[]>(query);
        }

        public async Task<IIKOCookingActDTO[]> GetCookingActs(DateTime dateFrom, DateTime dateTo)
        {
            // Пока так
            // TODO Сделать Builder, будем строить olap запросы и обращаться к данным через поля группировок и аггрегаций
            //
            var transactionTypeColumn = 0;
            var docNumberColumn = 1;
            var productIdColumn = 2;
            var productNameColumn = 3;
            var unitNameColumn = 4;
            var storeNameColumn = 5;
            var dateTimeColumn = 6;
            var amounColumn = 7;

            var query = $"{_host}/resto/service/reports/reports.jsp?reportId=transactionsOlapConfig" +
                $"&from={dateFrom.ToString("dd.MM.yyyy")}&to={dateTo.ToString("dd.MM.yyyy")}&group=TransactionType&group=Document" +
                $"&group=Product.Id&group=Product.Name&group=Product.MeasureUnit&group=Store&group=DateTime.Typed&agr=Amount";

            var response = await HttpBuilder
                .Create(query)
                .SetBasicAuthorization(_username, _password)
                .GetAsync();

            if (response.IsSuccessStatusCode == false)
            {
                throw new InvalidOperationException($"Не корректный запрос, статус ответа: {response.StatusCode}");
            }

            var parsedPage = ParseOlapPage(response.Data, x => x != null && x.Count() > 0 && x[transactionTypeColumn] == IIKOConst.OLAP_COOKING_ACT_NAME);

            return parsedPage
                .GroupBy(g => new
                {
                    number = g[docNumberColumn],
                    date = g[dateTimeColumn]
                })
                .Select(group => new IIKOCookingActDTO
            {
                DocumentNumber = group.Key.number,
                DateIncoming = DateTime.Parse(group.Key.date),
                Items = new IIKOCookingActItemsDTO
                {
                    Products = group.Select(item => new IIKOCookingActItemDTO 
                    { 
                        Amount = decimal.Parse(item[amounColumn]),
                        ProductId = Guid.Parse(item[productIdColumn]),
                        StoreName = item[storeNameColumn],
                        UnitMeasureName = item[unitNameColumn]
                    }).ToArray()
                }
            }).ToArray();
        }

        private string[][] ParseOlapPage(string page, Func<string[], bool> predicate = null)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(page);
            var result = doc.DocumentNode.Descendants()
                .Where(x => x.Attributes["class"]?.Value == "Data")
                .Select(table =>
                    table.Descendants("tr")
                    .Select(tr =>
                        tr.Descendants("td")
                            .Select(td => td.Descendants()
                            .Select(s => s.InnerText.Trim())
                            .FirstOrDefault())
                            .ToArray()))
                            ?.FirstOrDefault()
                            ?.Where(predicate)
            .ToArray();

            return result;
        }

        private Task LogoutServerAPI(string url, string code)
        {
            var authUrl = $"{url}/resto/api/logout?key={code}";
            return HttpBuilder
                .Create(authUrl)
                .GetAsync();
        }

        private async Task<string> AuthorizeServerAPI(string url, string username, string password)
        {
            var pswd = Crypto.GetHash(password, HashType.SHA1);
            var authUrl = $"{url}/resto/api/auth?login={username}&pass={pswd}";
            var response = await HttpBuilder
                .Create(authUrl)
                .GetAsync();

            return response.Data;
        }

        private Task<TIIKOType> LoadJson<TIIKOType>(string query)
            where TIIKOType : class
        {
            return Load<TIIKOType>(query, true);
        }

        private Task<TIIKOType> LoadXml<TIIKOType>(string query)
            where TIIKOType : class
        {
            return Load<TIIKOType>(query, false);
        }

        private async Task<TIIKOType> Load<TIIKOType>(string query, bool IsJson)
            where TIIKOType : class
        {
            if (string.IsNullOrWhiteSpace(_host))
            {
                throw new InvalidOperationException("Не указан url сервера iiko!");
            }

            var code = await AuthorizeServerAPI(_host, _username, _password);
            try
            {
                var response = await HttpBuilder
                    .Create($"{_host}{query}&key={code}")
                    .GetAsync();


                var jsonModel = IsJson
                    ? JsonConvert.DeserializeObject<TIIKOType>(response.Data)
                    : XML.Deserialize<TIIKOType>(response.Data);

                return jsonModel;
            }
            finally
            {
                await LogoutServerAPI(_host, code);
            }
        }
    }
}
